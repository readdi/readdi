#!/usr/bin/env python3
from optparse import OptionParser
import sys
import os
import math
from collections import defaultdict
from maxmatching import bipartiteMatch
from similarity import *
from overlap import overlap
from svcompare import graph_from_list
from svcompare import open_matching_file
from svcompare import close_matching_file
from svcompare import write_matching

usage = """%prog [options] <deletions1.(bed|vcf|...)> <deletions2.(bed|vcf|...)>

	Outputs true and false positives and negatives in terms of 50% pairwise overlap for lists deletions1 and deletions2."""

#def graph_from_list(list):
	#graph = {}
	#for i1, i2 in list:
		#if i1 not in graph:
			#graph[i1]=[i2]
		#else:
			#graph[i1].append(i2)
	#return graph


#def open_matching_file(filename):
	#"""Initializes the file to write the matching to."""
	#if filename == "":
		#return None
	#else:
		#f=open(os.path.expanduser(filename), 'w')
		#return f
	
#def close_matching_file(file):
	#"""closes the file to write the matching to."""
	#if not file is None:
		#file.close

#def write_matching(filename, deletion_list1, deletion_list2, chromosome, matching):
	#"""Writes matching deletions into the given file. """
	#if not filename is None:
		#for v in matching:
			#u=matching[v]
			#print(chromosome, deletion_list1[u], deletion_list2[v], sep='\t', file=filename)


def all_overl_indels(indel_list1,indel_list2):
	result=set()
	for i, indel1 in enumerate(indel_list1):
		for j, indel2 in enumerate(indel_list2):
			if indel_list1==indel_list2 or indel1<indel2:
				# 50% overlap?
				if overlap(indel1,indel2,0.5):
					result.add((indel1,indel2))
	return result


def compute_statistics(indels1, indels2, reference, chromosomes, ambiguous,  matching_file=None):
	"""computes TP, FP, FN, SP, SN, FNC, FPC for given lists of deletions considering only the specified chromosomes"""
	
	(deletions1, insertions1)=indels1
	(deletions2, insertions2)=indels2
		
	# init statistics
	total_stats_del=(0,0,0,0,0,0,0)
	total_stats_ins=(0,0,0,0,0,0,0)

	for chromosome in chromosomes:

		if not chromosome in reference:
			print('Skipping deletions for chromosome "%s": reference unknown.'%chromosome, file=sys.stderr)
			continue

		# DELETIONS
		print('Processing deletions on chromosome "%s".'%chromosome, file=sys.stderr)
		deletion_list1 = deletions1[chromosome]
		deletion_list2 = deletions2[chromosome]

		# do the matching and get statistics
		new_stats_del=compute_statistics_from_similarity_lists(deletion_list1, deletion_list2, all_overl_indels(deletion_list1,deletion_list2), all_overl_indels(deletion_list1,deletion_list1), all_overl_indels(deletion_list2,deletion_list2), chromosome, matching_file)
		total_stats_del = tuple(map(operator.add, total_stats_del, new_stats_del))


		# INSERTIONS
		print('Processing insertions on chromosome "%s".'%chromosome, file=sys.stderr)
		insertion_list1 = insertions1[chromosome]
		insertion_list2 = insertions2[chromosome]

		# do the matching and get statistics
		new_stats_ins=compute_statistics_from_similarity_lists(insertion_list1, insertion_list2, all_overl_indels(insertion_list1,insertion_list2), all_overl_indels(insertion_list1,insertion_list1), all_overl_indels(insertion_list2,insertion_list2), chromosome, matching_file)
		total_stats_ins = tuple(map(operator.add, total_stats_ins, new_stats_ins))



	return (total_stats_del,total_stats_ins)


if __name__ == '__main__':

	ts = time.time()

	# Parse Arguments with extra option
	args,options = parse_options(usage,3,[(["--matching"], {"action":"store", "dest":"matching_filename", "default":"", "type":"string", "help":"File to store the matching indels."})])

	matching_file=open_matching_file(options.matching_filename)

	#output header
	print("k1\tk2\tTP\tFP\tFN\tSP\tSN\tFNC\tFPC\tTIME\tTYPE")

	#output time info
	print('***','time:','k1','k2', sep="\t", file=sys.stderr)
	print('time for reading input:',time.time()-ts, sep="\t", file=sys.stderr)
	ts = time.time()
	
	indels1 = read_indels(args[1], options.inputformat1, k1, options.ambiguous)
	indels2 = read_indels(args[2], options.inputformat2, k2, options.ambiguous)
	
	(deletions1, insertions1, ins_sequence_given1)=indels1
	(deletions2, insertions2, ins_sequence_given2)=indels2

	
	# set of chromosome names appearing in both indel lists
	chromosomes = set(deletions1.keys()).intersection(set(deletions2.keys())).union(set(insertions1.keys()).union(set(insertions2.keys())))
	
	#DELETIONS
	#compute statistics 
	((TP_del,FP_del,FN_del,SP_del,SN_del,FNC_del,FPC_del),(TP_ins,FP_ins,FN_ins,SP_ins,SN_ins,FNC_ins,FPC_ins)) = compute_statistics(indels1, indels2, reference, chromosomes, options.ambiguous, matching_file)
	print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format("-","-",TP_del,FP_del,FN_del,SP_del,SN_del,FNC_del,FPC_del,"NA","DEL"))
	print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format("-","-",TP_ins,FP_ins,FN_ins,SP_ins,SN_ins,FNC_ins,FPC_ins,"NA","INS"))


	# BOTH
	print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(k1,k2,TP_del+TP_ins,FP_del+FP_ins,FN_del+FN_ins,SP_del+SP_ins,SN_del+SN_ins,FNC_del+FNC_ins,FPC_del+FPC_ins,time.time()-ts,"BOTH"))
	ts = time.time()


	close_matching_file(matching_file)
