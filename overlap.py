#!/usr/bin/env python3
from optparse import OptionParser
import sys
import os
import math
from collections import defaultdict
from maxmatching import bipartiteMatch
from similarity import *

usage = """%prog [options] <indels1.(bed|vcf|...)> <indels.(bed|vcf|...)>

Outputs a list of indels from lists indels1 and indels2 with a reciprocal overlap of at least 50%.
	If both files are the same, line n is not compared to itself."""

def overlap(indel1, indel2, ratio):
	"""Determines whether two indels overlap by the given ratio or not."""
	overlap=min(indel2.end,indel1.end)-max(indel1.start,indel2.start)
	len1=indel1.end-indel1.start
	len2=indel2.end-indel2.start
	return (overlap/len1>=ratio and overlap/len2>=ratio)
	

if __name__ == '__main__':

	# Parse Arguments
	args,options = parse_options(usage,2)

	(deletions1, insertions1,_) = read_indels(args[0], options.inputformat1, "0")
	(deletions2, insertions2,_) = read_indels(args[1], options.inputformat2, "0")
	chromosomes = set(deletions1.keys()).intersection(set(deletions2.keys())).union(set(insertions1.keys()).union(set(insertions2.keys())))
	
	for chromosome in chromosomes:
		print('Processing deletions on chromosome "%s".'%chromosome, file=sys.stderr)

		deletion_list1 = deletions1[chromosome]
		deletion_list2 = deletions2[chromosome]

		for i, del1 in enumerate(deletion_list1):
			for j, del2 in enumerate(deletion_list2):
				if args[0]!=args[1] or del1<del2:
					# 50% overlap?
					if overlap(del1,del2,0.5):
						print(chromosome, deletion_list1[i], deletion_list2[j], "DEL", sep='\t')

		insertion_list1 = insertions1[chromosome]
		insertion_list2 = insertions2[chromosome]

		for i, ins1 in enumerate(insertion_list1):
			for j, ins2 in enumerate(insertion_list2):
				if args[0]!=args[1] or ins1<ins2:
					# 50% overlap?
					if overlap(ins1,ins2,0.5):
						print(chromosome, insertion_list1[i], insertion_list2[j], "INS", sep='\t')

