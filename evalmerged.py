#!/usr/bin/env python3
from optparse import OptionParser
import sys
import os
import math
from collections import defaultdict
from maxmatching import bipartiteMatch
from similarity import *

usage = """%prog [options] <deletions1.(bed|vcf|...)> <deletions2.(bed|vcf|...)>

Comparison of bases by which the given deletions differ/agree. Consideres deletions1 to be The Truth. Outputs total1, total2, totalTP, totalFP, totalFN, precision, recall, number dels1, number dels2"""

def eval(deletions1, deletions2, chromosomes, indeltype):
	
	total1=0
	total2=0
	totalTP=0
	totalFP=0
	totalFN=0
	num1=0
	num2=0
	
	for chromosome in chromosomes:
		print('Processing deletions on chromosome "%s".'%chromosome, file=sys.stderr)

		deletion_list1 = deletions1[chromosome]
		deletion_list2 = deletions2[chromosome]
		
		num1+=deletion_list1.__len__()
		num2+=deletion_list2.__len__()
		

		set1=set()
		for d in deletion_list1:
			set1.update(range(d.start,d.end))
		
		total1+=set1.__len__()
			
		set2=set()
		for d in deletion_list2:
			set2.update(range(d.start,d.end))
		
		total2+=set2.__len__()

		fn=set1-set2
		fp=set2-set1
		tp=set1&set2
		
		totalFN+=fn.__len__()
		totalFP+=fp.__len__()
		totalTP+=tp.__len__()
		
	print(total1, total2, totalTP, totalFP, totalFN, "NA" if total2==0 else totalTP/total2, "NA" if total1==0 else totalTP/total1, num1 , num2, indeltype, sep="\t")



if __name__ == '__main__':

	# Parse Arguments
	args,options = parse_options(usage,2)

	indels1 = read_indels(args[0], options.inputformat1, "0", True)
	indels2 = read_indels(args[1], options.inputformat2, "0", True)
	
	(deletions1, insertions1, ins_sequence_given1)=indels1
	(deletions2, insertions2, ins_sequence_given2)=indels2

	# set of chromosome names appearing in both indel lists
	chromosomes = set(deletions1.keys()).intersection(set(deletions2.keys())).union(set(insertions1.keys()).union(set(insertions2.keys())))
	
	eval(deletions1, deletions2, chromosomes, "DEL")
	eval(insertions1, insertions2, chromosomes, "INS")




	