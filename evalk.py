#!/usr/bin/env python3
from optparse import OptionParser
import sys
import os
import math
import time
from collections import defaultdict
from maxmatching import bipartiteMatch
from similarity import *
from svcompare import *
import pylab as pl
import numpy as np


usage = """%prog [options] <reference.fasta> <prediction.(bed|vcf|...)> <truth.(bed|vcf|...)>
	Computes statistics on precision, recall and F-measure in terms of (k1,k2)-similarity 
	for different values of k1 and tries to suggest a reasonalbe value for k1 for further comparisons.
	"""


def get_prec_recall(pred, truth, reference, chromosomes, ambiguous):
	"""Computes precision and recall for given similarity values."""
	
	((TP_del,FP_del,FN_del,SP_del,SN_del,FNC_del,FPC_del),(TP_ins,FP_ins,FN_ins,SP_ins,SN_ins,FNC_ins,FPC_ins)) = compute_statistics(pred, truth, reference, chromosomes, ambiguous, None)

	prec = (TP_del + TP_ins) / (TP_del + TP_ins + FPC_del + FPC_ins)
	recall = (TP_del + TP_ins) / (TP_del + TP_ins + FNC_del + FNC_ins)
	prec_del = TP_del / (TP_del + FPC_del)
	recall_del = TP_del / (TP_del + FNC_del)
	prec_ins = TP_ins / (TP_ins + FPC_ins)
	recall_ins = TP_ins / (TP_ins + FNC_ins)
	return((prec, recall),(prec_del, recall_del), (prec_ins,recall_ins))


def get_F_measure(tuple):
	"""Returns F-measure for of tuple (precision, recall)."""

	(prec,recall)=tuple
	if (prec+recall)==0: return 0.0
	return(2*(prec*recall)/(prec+recall))

def plot_statistics(statistics, saturation_value, fac, k1, k2, prefix):
	"""Creates plot of statistics in PDF file"""

	#prepare values to plot
	ks=sorted(statistics.keys())
	fs=[]
	ps=[]
	rs=[]
	fs_del=[]
	ps_del=[]
	rs_del=[]
	fs_ins=[]
	ps_ins=[]
	rs_ins=[]
	for k in ks:
		fs.append(get_F_measure(statistics[k][0]))
		ps.append(statistics[k][0][0])
		rs.append(statistics[k][0][1])
		fs_del.append(get_F_measure(statistics[k][1]))
		ps_del.append(statistics[k][1][0])
		rs_del.append(statistics[k][1][1])
		fs_ins.append(get_F_measure(statistics[k][2]))
		ps_ins.append(statistics[k][2][0])
		rs_ins.append(statistics[k][2][1])
	
	# BOTH
	
	#prepare plot
	frame=pl.figure(figsize=(8,6))
	p=frame.add_subplot(111)
	pl.ylim(0,1)
	#pl.ylabel("")
	pl.xlabel("k_pred (k_truth = %s)"%(k2))
	#pl.title("")
	
	#plot
	p.set_position([0.1,0.1,0.6,0.8])
	p.plot(ks, fs, color="blue", linestyle='-', marker='x', label="F-measure")
	p.plot(ks, ps, color="blue", linestyle='--', marker='x', label="Precision")
	p.plot(ks, rs, color="blue", linestyle=':', marker='x', label="Recall")
	p.plot([0,max(ks)],[saturation_value,saturation_value], color="red",label="saturation")
	p.plot([0,max(ks)],[fac*saturation_value,fac*saturation_value], color="orange",label=str(fac))
	p.plot([k1,k1],[0,1], color="orange",label="suggested k")
	p.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

	pl.savefig(filename=prefix+".pdf",format="pdf")
	pl.close()
	
	# DELETIONS	

	if statistics[0][1]:
		
		#prepare plot
		frame=pl.figure(figsize=(8,6))
		p=frame.add_subplot(111)
		pl.ylim(0,1)
		#pl.ylabel("")
		pl.xlabel("k_pred (k_truth = %s)"%(k2))
		pl.title("Deletions only")
		
		#plot
		p.set_position([0.1,0.1,0.6,0.8])
		p.plot(ks, fs_del, color="red", linestyle='-', marker='x', label="F-measure")
		p.plot(ks, ps_del, color="red", linestyle='--', marker='x', label="Precision")
		p.plot(ks, rs_del, color="red", linestyle=':', marker='x', label="Recall")
		p.plot([k1,k1],[0,1], color="orange",label="suggested k")
		p.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

		pl.savefig(filename=prefix+"_deletions.pdf",format="pdf")
		pl.close()
	
	
	# INSERTIONS

	if statistics[0][2]:

		#prepare plot
		frame=pl.figure(figsize=(8,6))
		p=frame.add_subplot(111)
		pl.ylim(0,1)
		#pl.ylabel("")
		pl.xlabel("k_pred (k_truth = %s)"%(k2))
		pl.title("Insertions only")
		
		#plot
		p.set_position([0.1,0.1,0.6,0.8])
		p.plot(ks, fs_ins, color="green", linestyle='-', marker='x', label="F-measure")
		p.plot(ks, ps_ins, color="green", linestyle='--', marker='x', label="Precision")
		p.plot(ks, rs_ins, color="green", linestyle=':', marker='x', label="Recall")
		p.plot([k1,k1],[0,1], color="orange",label="suggested k")
		p.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

		pl.savefig(filename=prefix+"_insertions.pdf",format="pdf")
		pl.close()




def write_statsfile(filename):
	"""Writes the obtained statistics into the given file (tab-separated)"""
	f=open(os.path.expanduser(filename), 'w')
	print("k","Precision","Recall","F_measure","Type", sep='\t', file=f)
	for k in sorted(statistics.keys()):
		print(k, statistics[k][0][0], statistics[k][0][1] , get_F_measure(statistics[k][0]), "BOTH", sep='\t', file=f)
		print(k, statistics[k][1][0], statistics[k][1][1] , get_F_measure(statistics[k][1]), "DEL", sep='\t', file=f)
		print(k, statistics[k][2][0], statistics[k][2][1] , get_F_measure(statistics[k][2]), "INS", sep='\t', file=f)
	f.close()
	
	
if __name__ == '__main__':

	# Parse Arguments with extra option
	args,options = parse_options(usage,3,[
		(["--plotfile"], {"action":"store", "dest":"plotfile", "default":None, "type":"string", "help":"Prefix of file name for plots in PDF format. Default: No plot."}),
		(["--statsfile"], {"action":"store", "dest":"statsfile", "default":None, "type":"string", "help":"File name for statistics in tab-separated format. Default: No statistics."}),
		(["--chr"], {"action":"store", "dest":"chr", "default":"", "type":"string", "help":"List of chromosomes to be considered in the evaluation, e.g. \"2\" or \"1,5,Y\". Default: all."}),
		(["--k2"], {"action":"store", "dest":"k2", "default":"10", "type":"string", "help":"Allowed flexibility for indels in truth set. (Default 10)"}),
		(["--max_step"], {"action":"store", "dest":'max_step', "default":"50", "type":"int", "help":"Maximum step size for k1 when searching for saturation. (Default 50)"}),
		(["--precision"], {"action":"store", "dest":'prec', "default":"10", "type":"int", "help":"Optimal k1 is determined up to this precision. (Default 10)"}),
		(["--k_max"], {"action":"store", "dest":'k_max', "default":"1000", "type":"int", "help":"If k1 exceeds this threshold during the search for saturation, the search is aborted. (Default 1000)"}),
		(["--sat_fac"], {"action":"store", "dest":'sat_fac', "default":"0.0001", "type":"float", "help":"If F-measure does increase by a factor smaller than this, saturation is said to be reached. (Default 0.0001)"}),
		(["--min_fac"], {"action":"store", "dest":'min_fac', "default":"0.95", "type":"float", "help":"Optimal k1 is defined as the minimum k such that the F-measure exceeds this ratio of saturation. (Default 0.95)"})
		])

	if options.chr != "":
		if options.chr.find(",")>=0:
			chromosomes_predef=set(options.chr.replace('chr','').split(','))
		else:
			chromosomes_predef=set(options.chr)
	else:
		chromosomes_predef=None

	# Read indel sets
	# tuple each: (deletions, insertions)
	pred = read_indels(args[1], options.inputformat1, "0", options.ambiguous, chromosomes_predef, False)
	truth = read_indels(args[2], options.inputformat2, options.k2, options.ambiguous, chromosomes_predef, False)

	# Compose list of chromosomes
	chromosomes=set(pred[0].keys()).intersection(set(truth[0].keys())).union(set(pred[1].keys()).intersection(set(truth[1].keys())))
	
	
	# Read reference sequence
	reference=read_reference(args[0], chromosomes)

	# dict to store: k1->(Prec,Recall,F-measure)
	statistics = {}

	# Compute statistics for (0,k_truth)
	statistics[0]=get_prec_recall(pred, truth, reference, chromosomes, options.ambiguous)
	val_0 = get_F_measure(statistics[0][0])
	
	print("Statistics_both (Prec., Recall, F-measure) for k=","0", statistics[0][0][0], statistics[0][0][1] , get_F_measure(statistics[0][0]), file=sys.stderr)
	print("Statistics_del (Prec., Recall, F-measure) for k=","0", statistics[0][1][0], statistics[0][1][1] , get_F_measure(statistics[0][1]), file=sys.stderr)
	print("Statistics_ins (Prec., Recall, F-measure) for k=","0", statistics[0][2][0], statistics[0][2][1] , get_F_measure(statistics[0][2]), file=sys.stderr)


	# initial k
	k = options.prec
	
	# Compute statistics for (k1,k_truth)
	pred = read_indels(args[1], options.inputformat1, str(k), options.ambiguous, chromosomes, True)
	statistics[k]=get_prec_recall(pred, truth, reference, chromosomes, options.ambiguous)
	val_1 = get_F_measure(statistics[k][0])
	
	if val_0 > 0 :
		increase = val_1 / val_0  - 1
	else:
		increase = options.prec;

	# Repeat with increasing k until saturation is reached
	# increasing k according to the factor the F-measure increases (squared)
	# until factor below saturation_factor => saturation_value.
	# If k exceeds k_max => exit(1)
	
	val_prev = val_1
	k_diff=k-0

	print("Statistics_both (Prec., Recall, F-measure) for k=",k, statistics[k][0][0], statistics[k][0][1] , get_F_measure(statistics[k][0]), file=sys.stderr)
	print("Statistics_del (Prec., Recall, F-measure) for k=",k, statistics[k][1][0], statistics[k][1][1] , get_F_measure(statistics[k][1]), file=sys.stderr)
	print("Statistics_ins (Prec., Recall, F-measure) for k=",k, statistics[k][2][0], statistics[k][2][1] , get_F_measure(statistics[k][2]), file=sys.stderr)

	while (increase/k_diff >= options.sat_fac):
		
		# increase k
		k_prev=k
		k = min( max( k+options.prec , int(k*(1+increase)**2)), k+options.max_step ) // options.prec * options.prec
		k_diff = k - k_prev
		
		if k>options.k_max:
			print("*** k1 exceeded maximum value %d that  is specified by option --k_max"%(options.k_max), file=sys.stderr)
			exit(1)
		
		# compute and store new value
		pred = read_indels(args[1], options.inputformat1, str(k), options.ambiguous, chromosomes, True)
		statistics[k]=get_prec_recall(pred, truth, reference, chromosomes, options.ambiguous)
		val_new = get_F_measure(statistics[k][0])
		increase = val_new / val_prev - 1
		
		val_prev = val_new

		print("Statistics_both (Prec., Recall, F-measure) for k=",k, statistics[k][0][0], statistics[k][0][1] , get_F_measure(statistics[k][0]), file=sys.stderr)
		print("Statistics_del (Prec., Recall, F-measure) for k=",k, statistics[k][1][0], statistics[k][1][1] , get_F_measure(statistics[k][1]), file=sys.stderr)
		print("Statistics_ins (Prec., Recall, F-measure) for k=",k, statistics[k][2][0], statistics[k][2][1] , get_F_measure(statistics[k][2]), file=sys.stderr)

		
	# END_WHILE
	

	saturation_value = get_F_measure(statistics[k][0])
	
	print('Saturation value %f reached for k=%d with last increase of %f.'%(saturation_value,k,increase), file=sys.stderr)
							
	# binary search for minimum k1 s.th. F-measure >= min_fac * saturation_value := val_threshold
	val_threshold = options.min_fac * saturation_value
	
	k_below=0
	k_above=k
	for i in statistics.keys():
		if get_F_measure(statistics[i][0]) <= val_threshold and i > k_below:
			k_below = i
		if get_F_measure(statistics[i][0]) > val_threshold and i < k_above:
			k_above = i

	while(k_above-k_below>options.prec):
		k_mid=int(((k_below + k_above)/2) // options.prec * options.prec)
		pred = read_indels(args[1], options.inputformat1, str(k_mid), options.ambiguous, chromosomes, True)
		statistics[k_mid]=get_prec_recall(pred, truth, reference, chromosomes, options.ambiguous)
		val_new = get_F_measure(statistics[k_mid][0])
		if val_new <= val_threshold:
			k_below = k_mid
		else:
			k_above = k_mid

		print("Statistics_both (Prec., Recall, F-measure) for k=",k_mid, statistics[k_mid][0][0], statistics[k_mid][0][1] , get_F_measure(statistics[k_mid][0]), file=sys.stderr)
		print("Statistics_del (Prec., Recall, F-measure) for k=",k_mid, statistics[k_mid][1][0], statistics[k_mid][1][1] , get_F_measure(statistics[k_mid][1]), file=sys.stderr)
		print("Statistics_ins (Prec., Recall, F-measure) for k=",k_mid, statistics[k_mid][2][0], statistics[k_mid][2][1] , get_F_measure(statistics[k_mid][2]), file=sys.stderr)

	print('%f * saturation_value = %f reached for k=%d with value %f. (Precision of k =%d) '%(options.min_fac, val_threshold, k_above, val_new, options.prec), file=sys.stderr)

	print('Proposed k:', k_above)
	
	# output statistics
	
	if options.plotfile != None:
		plot_statistics(statistics, saturation_value, options.min_fac, k_above, options.k2, options.plotfile)
	
	if options.statsfile != None:
		write_statsfile(options.statsfile)
		
		
### R commands

#c<-read.table(file="evalk.clever.0.99.log", head=True)
#b<-read.table(file="evalk.breakdancer.0.99.log", head=True)
#p<-read.table(file="evalk.pindel.0.99.log", head=True)

#pdf(file="evalk.plot.0.99.pdf")
#plot(b$k,b$Precision,type="b", col="red", pch=15, ylim=c(0,1),ylab="prec/recall",xlab="k", main="threshold = 0.99, precision 10")
#points(b$k,b$Recall,type="b", col="red", pch=0)
#points(c$k,c$Precision,type="b", col="blue", pch=17)
#points(c$k,c$Recall,type="b", col="blue", pch=2)
#points(p$k,p$Precision,type="b", col="darkgreen", pch=18)
#points(p$k,p$Recall,type="b", col="darkgreen", pch=5)
#legend(x=140,y=0.5,legend=c("Breakdancer","Clever","Pindel"),col=c("red","blue","darkgreen"),lwd=1)
#dev.off()

#pdf(file="evalk.F.plot.0.99.pdf")
#plot(b$k,b$F_measure,type="b", col="red", pch=15, ylim=c(0,1),ylab="F-measure",xlab="k", main="threshold = 0.99, precision 10")
#points(c$k,c$F_measure,type="b", col="blue", pch=17)
#points(p$k,p$F_measure,type="b", col="darkgreen", pch=18)
#legend(x=140,y=0.5,legend=c("Breakdancer","Clever","Pindel"),col=c("red","blue","darkgreen"),lwd=1)
#dev.off()



### terminal commands

#for s in 0.9 0.95; 
#do 
#./evalk.py hg18.fa venter/clever.deletions20-10k.vcf venter/truth.deletions20-10k.vcf --chr=1,2 --min_fac=$s --precision=10 | tee evalk.clever.$s.log;
#./evalk.py hg18.fa venter/breakdancer.deletions20-10k.vcf venter/truth.deletions20-10k.vcf --chr=1,2 --min_fac=$s --precision=10 | tee evalk.breakdancer.$s.log;
#./evalk.py hg18.fa venter/pindel.deletions20-10k.vcf venter/truth.deletions20-10k.vcf --chr=1,2 --min_fac=$s --precision=10 | tee evalk.pindel.$s.log;
#done;
