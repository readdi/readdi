#!/usr/bin/env python3
from optparse import OptionParser
import sys
import os
import math
import re
import bisect
import time
from similarity_del import *
from similarity_ins import *
from similarity_misc import *
from collections import defaultdict
from Bio import SeqIO

usage = """%prog [options] <reference.fasta> <indels1.(bed|vcf|...)> <indels2.(bed|vcf|...)>

Outputs a list of k1-k2-similar indels from lists indels1 and indels2.
	If both deletion files are the same, line n is not compared to itself.
	If both deletion files are the same and also k1=k2, line n is only compared to lines i<n.
	Output in tab-separated format:
	Chrom.; Indel1: line no. in indel1, start, end, k1; Indel2: ...; (minimum shift, minimum required total flexibility); DEL|INS"""


class FileFormat:
	names = ['bed', 'vcf']
	def __init__(self, formatname, filename_hint=None):
		self.name=None
		if formatname == None:
			if filename_hint.endswith('.bed'):
				self.name = 'bed'
			elif filename_hint.endswith('.vcf'):
				self.name = 'vcf'
		else:
			if not formatname in FileFormat.names:
				raise Exception('Invalid format name: "%s", must be one of %s'%(formatname,', '.join('"'+s+'"' for s in FileFormat.names)))
			self.name = formatname


def insert_indel(indels, indel):
	"""Inserts given indel into the sorted list of indel. Before performing a binary insertion sort, 
	it is checked wether the new indel goes to the very end of the list. This is done to speed up the 
	insertion process in case all indels come in a sorted fashion."""
	if not indels or indel>indels[-1]:
		indels.append(indel)
	else:
		bisect.insort(indels, indel) 


def read_indels(filename, fileformat, default_k=None, ambi=False, chromosomes=None, silent=False):
	"""Reads indel from a BED file and returns a pair of dictionaries mapping chromosome names to
	a sorted list of deletions and insertions, resp."""
	if fileformat.name==None:
		raise Exception('Unknown input format for file %s. File must be a .bed file or .vcf file, or the format must be given explicitely with option --format(1|2).'%filename)		
	deletions = defaultdict(list)
	insertions = defaultdict(list)
	ins_sequence_given_error_printed = silent # if silent, then we just pretend that warning has been prtinted
	ins_sequence_given="?"
	if fileformat.name == 'vcf':
		info_and_k_warning_printed =  silent # if silent, then we just pretend that warning has been prtinted
		linenr = 0
		for line in open(filename):
			try:
				linenr += 1
				if line.startswith('#'):
					continue
				fields = line.split()
				assert len(fields) >= 8
				chromosome, pos, id, ref, alt, qual, filter, info = fields[0], int(fields[1]), fields[2], fields[3], fields[4], fields[5], fields[6], fields[7]
				if ambi and not ins_sequence_given_error_printed and len(ref)==1 and len(alt)>1 and valid_dna_string(alt):
					ins_sequence_given_error_printed=True
					print('Warning: Line %d of "%s" specifies insertion sequence, but it will be ignored by parameter choice.'%(linenr,filename), file=sys.stderr)
					print('         Subsequent warnings will be suppressed', file=sys.stderr)
					
					
				
				if chromosome.lower().startswith('chr'):
					chromosome=chromosome[3:]
				if chromosomes!=None and chromosome not in chromosomes:
					continue
				info_dict = dict(s.split('=') for s in info.split(';') if '=' in s)
				if 'SVTYPE' in info_dict:
					start = pos
					if info_dict['SVTYPE'] != 'DEL' and info_dict['SVTYPE'] != 'INS': continue
					if not 'SVLEN' in info_dict:
						if not 'END' in info_dict:
							print('Warning: line %d of "%s" invalid (both SVLEN and END missing), skipping it'%(linenr,filename), file=sys.stderr)
							continue
						else:
							length = int(info_dict['END']) - start + 1
					else:
						length = int(info_dict['SVLEN'])
				else:
					if not valid_dna_string(ref) or not valid_dna_string(alt): 
						print('Warning: line %d of "%s" invalid (REF or ALT contains non DNA characters), skipping it'%(linenr,filename), file=sys.stderr)
						continue
					if (len(ref) > 1) and (len(alt) == 1):
						length = - ( len(ref) - 1)
						start = pos
					elif len(ref)==1 and len(alt)>1:
						length=len(alt)-1
						start= pos
					else:
						continue
				
				if length>0 and not ambi: #check mix of of given and not given insertion sequences
					if ins_sequence_given=="?":
						if len(ref)==1 and len(alt)>1 and valid_dna_string(alt):
							ins_sequence_given="Y"
						else:
							ins_sequence_given="N"
					elif ins_sequence_given=="N" and len(ref)==1 and len(alt)>1 and valid_dna_string(alt):
						print('Error1: Sequence given for some but not all insertions. No mix supported. Offending line:', linenr, file=sys.stderr)
						print(line, file=sys.stderr)
						sys.exit(1)
					elif ins_sequence_given=="Y" and not (len(ref)==1 and len(alt)>1 and valid_dna_string(alt)):
						print('Error2: Sequence given for some but not all insertions. No mix supported. Offending line:', linenr, file=sys.stderr)
						print(line, file=sys.stderr)
						sys.exit(1)

				
				if default_k != None:
					if ('CIPOS' in info_dict) or ('CIEND' in info_dict) or ('CILEN' in info_dict) or ('BPWINDOW' in info_dict):
						if not info_and_k_warning_printed:
							print('Warning: Line %d of "%s" gives information on deletion uncertainty, but is overridden by k given on command line.'%(linenr,filename), file=sys.stderr)
							print('         Subsequent warnings will be suppressed', file=sys.stderr)
							info_and_k_warning_printed = True
					if (length<0):
						insert_indel(deletions[chromosome],NeighborhoodDeletion(linenr, start, start + abs(length), default_k))
					elif (length>0):
						if not ambi and ins_sequence_given=="Y":
							insert_indel(insertions[chromosome],NeighborhoodInsertion(linenr, start, alt, default_k))
						else:
							insert_indel(insertions[chromosome],NeighborhoodAmbInsertion(linenr, start, abs(length), default_k))
				else:
					if ('CIPOS' in info_dict) and ('CIEND' in info_dict) and (not 'CILEN' in info_dict) and (not 'BPWINDOW' in info_dict) and length<0:
						l_pos, r_pos = (int(x) for x in info_dict['CIPOS'].split(','))
						assert l_pos <= r_pos
						l_end, r_end = (int(x) for x in info_dict['CIEND'].split(','))
						assert l_end <= r_end
						insert_indel(result[chromosome],FuzzyDeletion(linenr, start + l_pos, start + r_pos, start + abs(length) + l_end, start + abs(length) + r_end))
					elif ('CIPOS' in info_dict) and (not 'CILEN' in info_dict) and (not 'BPWINDOW' in info_dict) and length>0:
						if not ambi and ins_sequence_given=="Y":
							insert_indel(insertions[chromosome],NeighborhoodInsertion(linenr, start, alt, default_k))
						else:
							insert_indel(insertions[chromosome],FuzzyInsertion(linenr, l_pos, r_pos, abs(length), default_k))
					elif (not 'CIPOS' in info_dict) and (not 'CIEND' in info_dict) and ('CILEN' in info_dict) and ('BPWINDOW' in info_dict):
						min_length, max_length = (int(x) for x in info_dict['CILEN'].split(','))
						assert min_length <= max_length
						start_min, end_max = (int(x) for x in info_dict['BPWINDOW'].split(','))
						# convert from 1-based to 0-based
						start_min -= 1
						end_max -= 1
						assert start_min <= start < end_max, "POS is not in given BPWINDOW in line %d"%linenr
						assert min_length <= length < max_length, "Indel length not in length window given by CILEN in line %d"%linenr
						if (length<0):
							insert_indel(result[chromosome],WindowDeletion(linenr, start_min, end_max, min_length, max_length))
						elif (length>0):
							print('Error: Unsupported info field CILEN for insertion. Offending line:', linenr, file=sys.stderr)
							sys.exit(1)
					elif (length<0):
						insert_indel(deletions[chromosome],NeighborhoodDeletion(linenr, start, start + abs(length), "0"))
					elif (length>0):
						if not ambi and ins_sequence_given=="Y":
							insert_indel(insertions[chromosome],NeighborhoodInsertion(linenr, start, alt,  "0"))
						else:
							insert_indel(insertions[chromosome],FuzzyInsertion(linenr, l_pos, r_pos, abs(length),  "0"))
					else:
						print('Error: Invalid combination of CI info fields, expected either CIPOS+CIEND or CILEN+CIWINDOW. Offending line:', linenr, file=sys.stderr)
						sys.exit(1)
			except ValueError:
				raise Exception('Bad input file. Offending line: %s'%linenr)
	else:
		linenr = 0
		for line in open(filename):
			linenr += 1
			if line.startswith('#'): continue
			try:
				fields = line.split()
				if fileformat.name == 'bed':
					assert len(fields) >= 3, 'Bad input file. Offending line: %s'%linenr
					chromosome, start, end = fields[0], int(fields[1]), int(fields[2])
					if chromosome.lower().startswith('chr'):
						chromosome=chromosome[3:]
					if chromosomes!=None and chromosome not in chromosomes:
						continue
					assert start < end, 'Bad input file. Offending line: %s'%linenr
					if default_k == None:
						if len(fields) > 3:
							k = int(fields[3])
							if len(fields) > 4 and fields[4]=="INS":
								insert_indel(insertions[chromosome],NeighborhoodAmbInsertion(linenr, start, end-start, k))
							else:
								insert_indel(deletions[chromosome],NeighborhoodDeletion(linenr, start, end, k))
						else:
							print("Error reading file %s: either a global parameter --k(1|2) or at least 4 columns expected in line %s"%(filename,linenr), file=sys.stderr)
							sys.exit(1)
					else:
						if len(fields) > 4 and fields[4]=="INS":
							insert_indel(insertions[chromosome],NeighborhoodAmbInsertion(linenr, start, end-start, default_k))
						else:
							insert_indel(deletions[chromosome],NeighborhoodDeletion(linenr, start, end, default_k))
				else:
					assert False, 'This is a bug'
			except ValueError:
				raise Exception('Bad input file. Offending line: %s'%linenr)
			
	if not silent:
		if not ambi and ins_sequence_given=="N":
			print('Warning: User parameter requests for NOT ignoring insertion sequences, but no insertion sequences given in input data.', file=sys.stderr)
			print('         Thus insertion sequences will not be considered.', file=sys.stderr)
		print('%d deletions read from file %s.'%(sum(len(x) for y,x in deletions.items()),filename), file=sys.stderr)
		print('%d insertions read from file %s.'%(sum(len(x) for y,x in insertions.items()),filename), file=sys.stderr)
	return (deletions,insertions,ins_sequence_given=="Y")






allowed_dna_chars = set(['A','C','G','T','N','a','c','g','t','n'])

def valid_dna_string(s):
	chars = set(c for c in s)
	return chars.issubset(allowed_dna_chars)

      
def parse_options(usage,numberOfFiles,moreOptions=[]):
	"""Here the parameters are defined, verfied and read. Arguments and parsed parameters are returned. Additional options can be specified in a list."""
	parser = OptionParser(usage=usage)
	parser.add_option("--format1", action="store", dest='inputformat1', default=None, type=str, help='Input file format for file1, must be one of %s.'%', '.join('"'+s+'"' for s in FileFormat.names)) 
	parser.add_option("--format2", action="store", dest='inputformat2', default=None, type=str, help='Input file format for file2, must be one of %s.'%', '.join('"'+s+'"' for s in FileFormat.names)) 
	parser.add_option("--ignore_ins_seq", action="store_true", dest='ambiguous', default=False, help='Do not consider insertion sequence, position and length only. Default: False (Consider sequence if given).')
	for args, kwargs in moreOptions:
		parser.add_option(*args, **kwargs)
	(options, args) = parser.parse_args()
	if len(args) != numberOfFiles:
		parser.print_help()
		sys.exit(1)
	options.inputformat1 = FileFormat(options.inputformat1, args[len(args)-2])
	options.inputformat2 = FileFormat(options.inputformat2, args[len(args)-1])
	return args,options

def parse_options_k1k2(usage,numberOfFiles,moreOptions=[]):
	"""Here the parameters for a particular comparison of two data sets are read."""
	return parse_options(usage,numberOfFiles,[
		(["--k1"], {"action":"store", "dest":"k1", "default":None, "type":"string", "help":"Allowed flexibility for deletions in first file, i.e., |i-i'| + |j-j'| where a deletion [i,j] is changed into [i',j'] (default: 0). If --k1=p% is given, where p can be float or integer, the flexibility will be interpreted w.r.t. to each deletion length. However, we advice against using this option, as the ability of deletion callers to place deletions does not (really) depend on deletion size. If --k1=start,end,step is given, the analysis will be performed for all values of k in the interval [start, start+step, ... end-1]. Specifying k1 here overwrites any k specified in the input file."}),
		(["--k2"], {"action":"store", "dest":"k2", "default":None, "type":"string", "help":"Allowed flexibility for deletions in second file (default: 0). If --k2=k1, the same value(s) will be used as given in --k1.  Specifying k2 here overwrites any k specified in the input file."})
		]+moreOptions)

def read_reference(filename,chromosomes=None):
	"""Reads all sequences from the given fasta file and returns a map chr->sequence. All sequences are made uppr case."""
	assert not filename.endswith('gz'), 'Currently, reading a gzipped reference is not supported.'
	reference = {}
	for record in SeqIO.parse(filename,"fasta"):
		chromosome = record.id.split()[0]
		if chromosome.lower().startswith('chr'):
			chromosome=chromosome[3:]
		if chromosomes != None and chromosome not in chromosomes:
			continue
		print('Loaded chromosome "{}"'.format(chromosome), file=sys.stderr)
		reference[chromosome] = record.seq.upper()
		if chromosomes!=None and len(reference) == len(chromosomes):
			break
	return reference



def get_list_of_ks(k):
	"""If the given string contains one or two commas, a range is returned. Otherwise the given string in form of a list."""
	if k is None or k.find(",")<0:
		klist = [k]
	else:
		ksplit=k.split(',')
		if len(ksplit)==2:
			klist=[str(x) for x in range(int(ksplit[0]),int(ksplit[1]))]
		elif len(ksplit)==3:
			klist=[str(x) for x in range(int(ksplit[0]),int(ksplit[1]),int(ksplit[2]))]
		else:
			raise Exception('Bad format for k: %s'%k)
	return klist


if __name__ == '__main__':

	ts = time.time()

	# Parse Arguments
	stats_option=(["--stats"], {"action":"store_true", "dest":"stats", "default":"", "help":"Compute and output for each pair of similar deletions: minimum required total flexibility, and minimum shift."})

	args,options = parse_options_k1k2(usage,3,[stats_option])
	
	# iterate over various k if necessary
	# create a list of k values, even if no range is given
	k1list=get_list_of_ks(options.k1)
	k2list=get_list_of_ks(options.k2)

	# Read reference sequence
	reference=read_reference(args[0])
	
	#output time info
	print('***','time:','k1','k2', sep="\t", file=sys.stderr)
	print('***',time.time()-ts,'-','-', sep="\t", file=sys.stderr)
	ts = time.time()
	
	for k1 in k1list:
		if options.k2 == "k1":
			k2list=[k1]
		for k2 in k2list:
	
			# handle comparison of list with itself separately
			if args[1] == args[2]:
				
				(deletions1,insertions1,ins_sequence_given1) = read_indels(args[1], options.inputformat1, k1, options.ambiguous)
				
				if k1!=k2:
					(deletions2,insertions2,ins_sequence_given2) = read_indels(args[1], options.inputformat2, k2, options.ambiguous)
				else:
					(deletions2,insertions2,ins_sequence_given2) = (deletions1,insertions1,ins_sequence_given1)

				ambiguous=options.ambiguous or not ins_sequence_given1 or not ins_sequence_given2

				# set of chromosome names appearing in deletion or insertion list
				chromosomes = set(deletions1.keys()).union(set(insertions1.keys()))
	

				for chromosome in chromosomes:
					if not chromosome in reference:
						print('Skipping indels for chromosome "%s": reference unknown.'%chromosome, file=sys.stderr)
						continue

# Deletions

					print('Processing deletions on chromosome "%s".'%chromosome, file=sys.stderr)
					deletion_list1 = deletions1[chromosome]
					deletion_list2 = deletions2[chromosome]
					similar_deletions = set()
					# get all similar pairs for which the deletion with k1 is further left (or at same position)
					# if file1==file2, the result set might contain symmetrical entries like (a,b) and (b,a). So we store them in a canonical way in the set
					# Ups, this does not work, because we then lose the connection index (i1 or i2) <-> k1 or k2. So we just check whether (b,a) is already in the set before we add (a,b)
					if options.stats:
						for i1, i2, K, s in all_similar_deletions_leftfirst(reference[chromosome], deletion_list1, deletion_list2, options.stats):
							#if i1<i2:
								#similar_deletions.add((i1,i2,K,s))
							#else:
								#similar_deletions.add((i2,i1,K,s))
							if not (i2,i1,K,s) in similar_deletions:
								similar_deletions.add((i1,i2,K,s))
					else:
						for i2, i1 in all_similar_deletions_leftfirst(reference[chromosome], deletion_list1, deletion_list2, options.stats):
							#if i1<i2:
								#similar_deletions.add((i1,i2))
							#else:
								#similar_deletions.add((i2,i1))
							if not (i2,i1) in similar_deletions:
								similar_deletions.add((i1,i2))

					
					
					# get all similar pairs for which the deletion with k2 is further left (or at same position)
					# if k1=k2, these are equivalent to above
					if k1!=k2:
						if options.stats:
							similar_deletions.update((i1,i2,K,s) for i2, i1, K, s in all_similar_deletions_leftfirst(reference[chromosome], deletion_list2, deletion_list1, options.stats))
						else:
							similar_deletions.update((i1,i2) for i2, i1 in all_similar_deletions_leftfirst(reference[chromosome], deletion_list2, deletion_list1, options.stats))

					similar_deletions_list = list(similar_deletions)
					similar_deletions_list.sort()

# Insertions

					print('Processing insertions on chromosome "%s".'%chromosome, file=sys.stderr)
					insertion_list1 = insertions1[chromosome]
					insertion_list2 = insertions2[chromosome]
					
# Ambiguous insertions
					similar_amb_insertions = set()
					# get all similar pairs for which the insertion with k1 is further left (or at same position)
					# if file1==file2, the result set might contain symmetrical entries like (a,b) and (b,a). So we store them in a canonical way in the set
					# Ups, this does not work, because we then lose the connection index (i1 or i2) <-> k1 or k2. So we just check whether (b,a) is already in the set before we add (a,b)
					if ambiguous:
						if options.stats:
							for i1, i2, K, s in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list1, insertion_list2, options.stats, ambiguous):
								#if i1<i2:
									#similar_amb_insertions.add((i1,i2,K,s))
								#else:
									#similar_amb_insertions.add((i2,i1,K,s))
								if not (i2,i1,K,s) in similar_amb_insertions:
									similar_amb_insertions.add((i1,i2,K,s))
						else: 
							for i1, i2 in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list1, insertion_list2, options.stats, ambiguous):
								#if i1<i2:
									#similar_amb_insertions.add((i1,i2))
								#else:
									#similar_amb_insertions.add((i2,i1))
								if not (i2,i1) in similar_amb_insertions:
									similar_amb_insertions.add((i1,i2))
					else: #not amb, need l for filtering process
						for i1, i2, l in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list1, insertion_list2, options.stats, ambiguous):
							#if i1<i2:
								#similar_amb_insertions.add((i1,i2, l))
							#else:
								#similar_amb_insertions.add((i2,i1, l))
							if not (i2,i1,l) in similar_amb_insertions:
								similar_amb_insertions.add((i1,i2,l))

					
					
					# get all similar pairs for which the insertion with k2 is further left (or at same position)
					# if k1=k2, these are equivalent to above
					if k1!=k2:
						if ambiguous:
							if options.stats:
								similar_amb_insertions_inv.update((i2,i1,K,s) for i2, i1, K, s in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list1, options.stats, ambiguous))
							else: 
								similar_amb_insertions_inv.update((i2,i1) for i2, i1 in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list1, options.stats, ambiguous))
						else: #not amb, need l for filtering process
							similar_amb_insertions_inv = set()
							similar_amb_insertions_inv.update((i2,i1,l) for i2, i1, l in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list1, options.stats, ambiguous))
							pot_similar_insertions_list_inv=list(similar_amb_insertions_inv)
							pot_similar_insertions_list_inv.sort()


					pot_similar_insertions_list=list(similar_amb_insertions)
					pot_similar_insertions_list.sort()

					if ambiguous:
						similar_insertions_list=pot_similar_insertions_list
					else:
# Unambiguous insertions: filter previous results
						similar_insertions = set()
						# get all similar pairs for which the insertion with k1 is further left (or at same position)
						# if file1==file2, the result set might contain symmetrical entries like (a,b) and (b,a). So we store them in a canonical way in the set
						# Ups, this does not work, because we then lose the connection index (i1 or i2) <-> k1 or k2. So we just check whether (b,a) is already in the set before we add (a,b)
						if options.stats:
							for i1, i2, K, s in all_similar_insertions_leftfirst(reference[chromosome], pot_similar_insertions_list, insertion_list1, insertion_list2, options.stats, True):
								#if i1<i2:
									#similar_insertions.add((i1,i2,K,s))
								#else:
									#similar_insertions.add((i2,i1,K,s))
								if not (i2,i1,K,s) in similar_insertions:
									similar_insertions.add((i1,i2,K,s))
						else:
							for i1, i2 in all_similar_insertions_leftfirst(reference[chromosome], pot_similar_insertions_list, insertion_list1, insertion_list2, options.stats, True):
								#if i1<i2:
									#similar_insertions.add((i1,i2))
								#else:
									#similar_insertions.add((i2,i1))
								if not (i2,i1) in similar_insertions:
									similar_insertions.add((i1,i2))

						
						
						# get all similar pairs for which the insertion with k2 is further left (or at same position)
						# if k1=k2, these are equivalent to above
						if k1!=k2:
							if options.stats:
								similar_insertions.update((i1,i2,K,s) for i2, i1, K, s in all_similar_insertions_leftfirst(reference[chromosome], pot_similar_insertions_list_inv, insertion_list2, insertion_list1, options.stats, True))
							else:
								similar_insertions.update((i1,i2) for i2, i1 in all_similar_insertions_leftfirst(reference[chromosome], pot_similar_insertions_list_inv, insertion_list2, insertion_list1, options.stats, True))
						
						similar_insertions_list=list(similar_insertions)
						similar_insertions_list.sort()
# Output
					if options.stats:
						for i1, i2, K, s in similar_deletions_list:
							if i1!=i2: print(chromosome, deletion_list1[i1], deletion_list2[i2], s, K, "DEL", sep='\t')
						for i1, i2, K, s in similar_insertions_list:
							if i1!=i2: print(chromosome, insertion_list1[i1], insertion_list2[i2], s, K, "INS", sep='\t')
					else:
						for i1, i2 in similar_deletions_list:
							if i1!=i2: print(chromosome, deletion_list1[i1], deletion_list2[i2], "DEL", sep='\t')
						for i1, i2 in similar_insertions_list:
							if i1!=i2: print(chromosome, insertion_list1[i1], insertion_list2[i2], "INS", sep='\t')




			else: # file1 != file 2
				(deletions1,insertions1, ins_sequence_given1) = read_indels(args[1], options.inputformat1, k1, options.ambiguous)
				(deletions2,insertions2, ins_sequence_given2) = read_indels(args[2], options.inputformat2, k2, options.ambiguous)

				ambiguous=options.ambiguous or not ins_sequence_given1 or not ins_sequence_given2

				# set of chromosome names appearing in both deletion lists or in both insertion lists
				chromosomes = set(deletions1.keys()).intersection(set(deletions2.keys())).union(set(insertions1.keys()).union(set(insertions2.keys())))
				for chromosome in chromosomes:
					if not chromosome in reference:
						print('Skipping deletions for chromosome "%s": reference unknown.'%chromosome, file=sys.stderr)
						continue
# Deletions
					print('Processing deletions on chromosome "%s".'%chromosome, file=sys.stderr)
					deletion_list1 = deletions1[chromosome]
					deletion_list2 = deletions2[chromosome]
					similar_deletions = set()
					# get all similar pairs for which the deletion from list1 is further left (or at same position)
					similar_deletions.update(all_similar_deletions_leftfirst(reference[chromosome], deletion_list1, deletion_list2, options.stats))
					# get all similar pairs for which the deletion from list2 is further left (or at same position)
					# that means, the ones at the same position were already returned by last call to all_similar_deletions_leftfirst
					# (which does not matter as similar_deletions is a set).
					if options.stats:
						similar_deletions.update((i1,i2,K,s) for i2, i1, K, s in all_similar_deletions_leftfirst(reference[chromosome], deletion_list2, deletion_list1, options.stats))
					else:
						similar_deletions.update((i1,i2) for i2, i1 in all_similar_deletions_leftfirst(reference[chromosome], deletion_list2, deletion_list1, options.stats))
					similar_deletions_list = list(similar_deletions)
					similar_deletions_list.sort()
# Insertions
					print('Processing insertions on chromosome "%s".'%chromosome, file=sys.stderr)
					insertion_list1 = insertions1[chromosome]
					insertion_list2 = insertions2[chromosome]
					similar_amb_insertions = set()
					# get all ambiguously similar pairs for which the insertion from list1 is further left (or at same position)
					similar_amb_insertions.update(all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list1, insertion_list2, options.stats, ambiguous))
					# get all similar pairs for which the insertion from list2 is further left (or at same position)
					# that means, the ones at the same position were already returned by last call to all_similar_insertions_leftfirst
					# (which does not matter as similar_insertions is a set).
					if ambiguous:
						if options.stats:
							similar_amb_insertions.update((i1,i2,K,s) for i2, i1, K, s in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list1, options.stats, ambiguous))
						else:
							similar_amb_insertions.update((i1,i2) for i2, i1 in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list1, options.stats, ambiguous))
					else:
						similar_amb_insertions_inv = set()
						similar_amb_insertions_inv.update((i2,i1,l) for i2, i1, l in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list1, options.stats, ambiguous))
						pot_similar_insertions_list_inv = list(similar_amb_insertions_inv)
						pot_similar_insertions_list_inv.sort()
	
	
					pot_similar_insertions_list = list(similar_amb_insertions)
					pot_similar_insertions_list.sort()
					
					if ambiguous:
						similar_insertions_list=pot_similar_insertions_list
					else:
# Unambiguous insertions: filter previous results
						similar_insertions = set()
						similar_insertions.update(all_similar_insertions_leftfirst(reference[chromosome], pot_similar_insertions_list, insertion_list1, insertion_list2, options.stats, False))
						if options.stats:
							similar_insertions.update((i1,i2,K,s) for i2, i1, K, s in all_similar_insertions_leftfirst(reference[chromosome], pot_similar_insertions_list_inv, insertion_list2, insertion_list1, options.stats, False))
						else:
							similar_insertions.update((i1,i2) for i2, i1 in all_similar_insertions_leftfirst(reference[chromosome], pot_similar_insertions_list_inv, insertion_list2, insertion_list1, options.stats, False))
						
						similar_insertions_list=list(similar_insertions)
						similar_insertions_list.sort()

# Output
					# print similar pairs
					if options.stats:
						for i1, i2, K, s in similar_deletions_list:
							print(chromosome, deletion_list1[i1], deletion_list2[i2], s, K, "DEL", sep='\t')
						for i1, i2, K, s in similar_insertions_list:
							print(chromosome, insertion_list1[i1], insertionn_list2[i2], s, K, "INS", sep='\t')
					else:
						for i1, i2 in similar_deletions_list:
							print(chromosome, deletion_list1[i1], deletion_list2[i2], "DEL", sep='\t')
						for i1, i2 in similar_insertions_list:
							print(chromosome, insertion_list1[i1], insertion_list2[i2], "INS", sep='\t')
							
							
			#output time info for this k's
			print('***',time.time()-ts,k1,k2, sep="\t", file=sys.stderr)
			ts = time.time()

