#!/usr/bin/env python3
from optparse import OptionParser
import sys
import os
import math
import re
import bisect
import time
from collections import defaultdict
from Bio import SeqIO
from similarity_misc import *

def cmp(a,b):
	return (a > b) - (a < b)


class NeighborhoodDeletion:
	def __init__(self, id, start, end, k):
		"""Models a deletion where start and end are in "pythonic" coordinates. The parameter k defines 
		a k-neighborhood as follows:
		N_k(start,end) = { (start',end'): |start-start'|+|end-end'|<=k }."""
		self.id = id
		self.start = start
		self.end = end
		if k.isdigit():
			self.k = int(k)
		else:
			m = re.match(r"([\d\.]+).*",k)
			p = float(m.group(1))
			self.k = int(p*(end-start)/100)
	def __lt__(self, other): return self.__cmp__(other) == -1
	def __eq__(self, other): return self.__cmp__(other) == 0
	def __cmp__(self, other):
		return cmp(self.leftmostStartPos(), other.leftmostStartPos()) or cmp(self.id, other.id)
	def __str__(self):
		return '\t'.join(str(x) for x in [self.id,self.start,self.end,self.k])
	def leftmostStartPos(self):
		return max(0, self.start - self.k)
	def possibleLengths(self):
		length = self.end - self.start
		return range(max(1,length-self.k), length+self.k+1)
	def lengthValid(self, length):
		return max(1,self.end - self.start - self.k) <= length <= self.end - self.start + self.k
	def possibleStartPositions(self, length):
		"""Returns (start_min, start_max) of allowed start positions for a given length,
		i.e. all values of start with start_min <= start <= start_max are valid start positions
		of deletions of the given length (INCLUSIVE)."""
		assert self.lengthValid(length)
		return max(0, int(math.ceil((self.start+self.end-length-self.k)/2))),  int(math.floor((self.start+self.end-length+self.k)/2))

class FuzzyDeletion:
	def __init__(self, id, start_min, start_max, end_min, end_max):
		"""Models a deletion where start and end are lie in the given intervals, i.e.
		start_min <= start < start_max and 
		end_min <= end < end_max."""
		self.id = id
		self.start_min = start_min
		self.start_max = start_max
		self.end_min = end_min
		self.end_max = end_max
	def __lt__(self, other): return self.__cmp__(other) == -1
	def __eq__(self, other): return self.__cmp__(other) == 0
	def __cmp__(self, other):
		return cmp(self.leftmostStartPos(), other.leftmostStartPos()) or cmp(self.id, other.id)
	def __str__(self):
		return '\t'.join(str(x) for x in [self.id,self.start_min,self.start_max,self.end_min,self.end_max])
	def leftmostStartPos(self):
		return self.start_min
	def possibleLengths(self):
		return range(max(1,self.end_min - self.start_max), max(1, self.end_max - self.start_min) + 1) 
	def lengthValid(self, length):
		return max(1,self.end_min-self.start_max) <= length <= max(1,self.end_max-self.start_min)
	def possibleStartPositions(self, length):
		assert self.lengthValid(length)
		return max(self.start_min, self.end_min-length, 0), max(min(self.start_max, self.end_max - length), 0)
	def leftmostStartPos(self):
		return self.start_min

class WindowDeletion:
	def __init__(self, id, start_min, end_max, min_length, max_length):
		"""Models a deletion where
		start_min <= start
		end <= end_max
		min_length <= end - start <= max_length."""
		self.id = id
		self.start_min = start_min
		self.end_max = end_max
		self.min_length = min_length
		self.max_length = max_length
	def __lt__(self, other): return self.__cmp__(other) == -1
	def __eq__(self, other): return self.__cmp__(other) == 0
	def __cmp__(self, other):
		return cmp(self.leftmostStartPos(), other.leftmostStartPos()) or cmp(self.id, other.id)
	def __str__(self):
		return '\t'.join(str(x) for x in [self.id,self.start_min,self.end_max,self.min_length,self.max_length])
	def leftmostStartPos(self):
		return self.start_min
	def possibleLengths(self):
		return range(max(1,self.min_length), min(self.max_length, self.end_max-self.start_min)+1)
	def lengthValid(self, length):
		return max(1,self.min_length) <= length <= min(self.max_length, self.end_max-self.start_min)
	def possibleStartPositions(self, length):
		assert self.lengthValid(length)
		return self.start_min, self.end_max - length
	def leftmostStartPos(self):
		return self.start_min

def compute_stats(sequence, deletion1, deletion2):
	"""For a given pair of similar deletions, it returns the tupel (id of deletion 1, id of deletion 2, minimum required flexibility, minimum shift)."""
	# the latter two values are only defined for neighborhood deletions
	if not (deletion1.__class__.__name__=="NeighborhoodDeletion" and deletion2.__class__.__name__=="NeighborhoodDeletion"):
		return ("undef", "undef")
	min_K=None
	min_s=None
	# iterate over all lengths that can be in k1-neighborhood of [start1:end1] and k2-neighborhood of [start2:end2]
	for length in set(deletion1.possibleLengths()).intersection(set(deletion2.possibleLengths())):
		# compute range of possible start positions for this length
		unshifted_start_min1, unshifted_start_max1 = deletion1.possibleStartPositions(length)
		unshifted_start_min2, unshifted_start_max2 = deletion2.possibleStartPositions(length)
		# if start position too far right, we are done with this length
		shifted_start_max1 =  unshifted_start_max1 + lce(sequence, unshifted_start_max1, unshifted_start_max1 + length)
		if shifted_start_max1 < deletion2.leftmostStartPos(): continue
		# for del1, start with rightmost position of minimum k (if you could shift a deletion starting from a position further to the left, you will walk along this one anyway.)
		unshifted_start_start1 = max(deletion1.start,deletion1.end-length)
		# for del2, start with leftmost position of minimum k (if you could shift a deletion starting from a position further to the right, you will walk along this one anyway.)
		unshifted_start_start2 = min(deletion2.end-length,deletion2.start)
		# in case the deletions overlap, we cannot be such stringend
		if unshifted_start_start2 < unshifted_start_start1:
			unshifted_start_start2 = deletion2.start
			unshifted_start_start1 = deletion1.start
		# try different starts for del1
		for unshifted_start_1 in range(unshifted_start_start1,unshifted_start_max1+1):
			# compute used k1
			k1 = abs(unshifted_start_1-deletion1.start) + abs(unshifted_start_1+length - deletion1.end)
			# k1 already larger than minimum K? This and further starts cannot yield new optimum
			if not min_K==None and k1>min_K:
				break
			shifted_start_1 = unshifted_start_1 + lce(sequence, unshifted_start_1, unshifted_start_1 + length)
			# try different starts for del2
			for unshifted_start_2 in range(unshifted_start_start2,unshifted_start_min2-1,-1):
				# if start position is in range, these two deletions are similar
				if unshifted_start_2 <= shifted_start_1:
					# compute used k2 and total used K
					k2 = abs(unshifted_start_2-deletion2.start) + abs(unshifted_start_2+length - deletion2.end)
					K = k1+k2
					#hit -> update minima
					if min_K==None or K <= min_K:
						#compute shift
						s = abs(unshifted_start_2 - unshifted_start_1)
						if min_s==None or K < min_K or s < min_s:
							min_s=s
						min_K = K
	return (min_K, min_s)


def all_similar_deletions_leftfirst(sequence, list1, list2, stats):
	"""Given two sorted lists of deletions [(start,end),...] computes the set of all pairs of indices of 
	k1-k2-similar deletions for which the one in the first list is left of or at the same position as the
	one in the second list."""
	matches = set() # store just the matching pair
	result = set() # store more info
	if (len(list1) == 0) or (len(list2) == 0):
		return result
	# i2 gives the smallest index in list2 such that start2 >= start1, where
	# start1, end1 = list1[i1] and start2, end2 = list2[i2]
	i2 = 0
	deletion2 = list2[i2]
	for i1, deletion1 in enumerate(list1):
		while deletion2.leftmostStartPos() < deletion1.leftmostStartPos():
			if i2 + 1 < len(list2):
				i2 += 1
				deletion2 = list2[i2]
			else:
				# if there are no deletions in list2 right of [start1:end1], we are done
				return result
		# iterate over all lengths that can be in k1-neighborhood of [start1:end1]
		for length in deletion1.possibleLengths():
			# compute range of possible start positions
			unshifted_start_min1, unshifted_start_max1 = deletion1.possibleStartPositions(length)
			shifted_start_max1 =  unshifted_start_max1 + lce(sequence, unshifted_start_max1, unshifted_start_max1 + length)
			for j in range(i2, len(list2)):
				deletion_j = list2[j]
				# if start position too far right, we are done since all following deletion_j will be even further to the right
				if shifted_start_max1 < deletion_j.leftmostStartPos(): break
				# if length is not compatible, we can skip this one
				if not deletion_j.lengthValid(length): continue
				unshifted_start_min2, unshifted_start_max2 = deletion_j.possibleStartPositions(length)
				# if start position is in range, these two deletions are similar
				if unshifted_start_min2 <= shifted_start_max1:
					# the same pair can match several times with different lengths (and thus shifts), so check for uniqueness
					if not matches.__contains__((i1,j)):
						matches.add((i1,j))
						if stats:
							# compute minimum required sum of k1 and k2
							# compute minimum shift with minimum k1+k2
							(K,s)=compute_stats(sequence, deletion1, deletion_j)
							result.add((i1,j,K,s))
						else:
							result.add((i1, j))
	l = list(result)
	l.sort()
	return l


		

#def read_deletions(filename, fileformat, default_k=None, chromosomes=None):
	#"""Reads deletions from a BED file and returns a dictionary mapping chromosome names to
	#sorted lists of pairs of start and end coordinate."""
	#if fileformat.name==None:
		#raise Exception('Unknown input format for file %s. File must be a .bed file or .vcf file, or the format must be given explicitely with option --format(1|2).'%filename)		
	#result = defaultdict(list)
	#if fileformat.name == 'vcf':
		#info_and_k_warning_printed = False
		#linenr = 0
		#for line in open(filename):
			#try:
				#linenr += 1
				#if line.startswith('#'):
					#continue
				#fields = line.split()
				#assert len(fields) >= 8
				#chromosome, pos, id, ref, alt, qual, filter, info = fields[0], int(fields[1]), fields[2], fields[3], fields[4], fields[5], fields[6], fields[7]
				#if chromosomes!=None and chromosome not in chromosomes:
					#continue
				#info_dict = dict(s.split('=') for s in info.split(';') if '=' in s)
				#if chromosome.lower().startswith('chr'):
					#chromosome=chromosome[3:]
				#if (ref == '.') or (alt == '.') or (alt == '<DEL>'):
					#if 'SVTYPE' in info_dict:
						#del_start = pos
						#if info_dict['SVTYPE'] != 'DEL': continue
						#if not 'SVLEN' in info_dict:
							#if not 'END' in info_dict:
								#print('Warning: line %d of "%s" invalid (both SVLEN and END missing), skipping it'%(linenr,filename), file=sys.stderr)
								#continue
							#else:
								#del_length = int(info_dict['END']) - del_start + 1
						#else:
							#del_length = abs(int(info_dict['SVLEN']))
				#else:
					#if not valid_dna_string(ref) or not valid_dna_string(alt): 
						#print('Warning: line %d of "%s" invalid (REF or ALT contains non DNA characters), skipping it'%(linenr,filename), file=sys.stderr)
						#continue
					#if (len(ref) > 1) and (len(alt) == 1):
						#del_start = pos
						#del_length = len(ref) - 1
					#else:
						#continue
				#if default_k != None:
					#if ('CIPOS' in info_dict) or ('CIEND' in info_dict) or ('CILEN' in info_dict) or ('BPWINDOW' in info_dict):
						#if not info_and_k_warning_printed:
							#print('Warning: Line %d of "%s" gives information on deletion uncertainty, but is overridden by k given on command line.'%(linenr,filename), file=sys.stderr)
							#print('         Subsequent warnings will be suppressed', file=sys.stderr)
							#info_and_k_warning_printed = True
					#insert_deletion(result[chromosome],NeighborhoodDeletion(linenr, del_start, del_start + del_length, default_k))
				#else:
					#if ('CIPOS' in info_dict) and ('CIEND' in info_dict) and (not 'CILEN' in info_dict) and (not 'BPWINDOW' in info_dict):
						#l_pos, r_pos = (int(x) for x in info_dict['CIPOS'].split(','))
						#assert l_pos <= r_pos
						#l_end, r_end = (int(x) for x in info_dict['CIEND'].split(','))
						#assert l_end <= r_end
						#insert_deletion(result[chromosome],FuzzyDeletion(linenr, del_start + l_pos, del_start + r_pos, del_start + del_length + l_end, del_start + del_length + r_end))
					#elif (not 'CIPOS' in info_dict) and (not 'CIEND' in info_dict) and ('CILEN' in info_dict) and ('BPWINDOW' in info_dict):
						#min_length, max_length = (int(x) for x in info_dict['CILEN'].split(','))
						#assert min_length <= max_length
						#start_min, end_max = (int(x) for x in info_dict['BPWINDOW'].split(','))
						## convert from 1-based to 0-based
						#start_min -= 1
						#end_max -= 1
						#assert start_min <= del_start < end_max, "POS is not in given BPWINDOW in line %d"%linenr
						#assert min_length <= del_length < max_length, "Deletion length not in length window given by CILEN in line %d"%linenr
						#insert_deletion(result[chromosome],WindowDeletion(linenr, start_min, end_max, min_length, max_length))
					#else:
						#print('Error: Invalid combination of CI info fields, expected either CIPOS+CIEND or CILEN+CIWINDOW. Offending line:', linenr, file=sys.stderr)
						#sys.exit(1)
			#except ValueError:
				#raise Exception('Bad input file. Offending line: %s'%linenr)
	#else:
		#linenr = 0
		#for line in open(filename):
			#linenr += 1
			#if line.startswith('#'): continue
			#try:
				#fields = line.split()
				#if fileformat.name == 'bed':
					#assert len(fields) >= 3, 'Bad input file. Offending line: %s'%linenr
					#chromosome, start, end = fields[0], int(fields[1]), int(fields[2])
					#if chromosome.lower().startswith('chr'):
						#chromosome=chromosome[3:]
					#if chromosomes!=None and chromosome not in chromosomes:
						#continue
					#assert start < end, 'Bad input file. Offending line: %s'%linenr
					#if default_k == None:
						#if len(fields) > 3:
							#k = int(fields[3])
							#insert_deletion(result[chromosome],NeighborhoodDeletion(linenr, start, end, k))
						#else:
							#print("Error reading file %s: either a global parameter --k(1|2) or at least 4 columns expected in line %s"%(filename,linenr), file=sys.stderr)
							#sys.exit(1)
					#else:
						#insert_deletion(result[chromosome],NeighborhoodDeletion(linenr, start, end, default_k))
				#elif fileformat.name == 'neighborhood':
					#assert len(fields) >= 5
					#chromosome, start_min, start_max, end_min, end_max = fields[0], int(fields[1]), int(fields[2]), int(fields[3]), int(fields[4])
					#if chromosome.lower().startswith('chr'):
						#chromosome=chromosome[3:]
					#if chromosomes!=None and chromosome not in chromosomes:
						#continue
					#assert (start_min < start_max) and (end_min < end_max) , 'Bad input file. Offending line: %s'%linenr
					#assert (start_min < end_max), 'Bad input file. Offending line: %s'%linenr
					#insert_deletion(result[chromosome],FuzzyDeletion(linenr, start_min, start_max, end_min, end_max))
				#elif fileformat.name == 'windows':
					#assert len(fields) >= 5
					#chromosome, start_min, end_max, min_length, max_length = fields[0], int(fields[1]), int(fields[2]), int(fields[3]), int(fields[4])
					#if chromosome.lower().startswith('chr'):
						#chromosome=chromosome[3:]
					#if chromosomes!=None and chromosome not in chromosomes:
						#continue
					#assert 0 < min_length <= max_length, 'Bad input file. Offending line: %s'%linenr
					#assert start_min < end_max, 'Bad input file. Offending line: %s'%linenr
					#assert end_max - start_min >= min_length, 'Bad input file. Offending line: %s'%linenr
					#insert_deletion(result[chromosome],WindowDeletion(linenr, start_min, end_max, min_length, max_length))
				#else:
					#assert False, 'This is a bug'
			#except ValueError:
				#raise Exception('Bad input file. Offending line: %s'%linenr)
	#print('%d deletions read from file %s.'%(sum(len(x) for y,x in result.items()),filename), file=sys.stderr)
	#return result
      

