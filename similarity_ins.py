#!/usr/bin/env python3
from __future__ import print_function
from optparse import OptionParser
import sys
import math
import re
import bisect
from collections import defaultdict
from similarity_misc import *


class NeighborhoodAmbInsertion:
	def __init__(self, id, index, length, k):
		"""Models an ambiguous insertion where index and length are in "pythonic" coordinates. The parameter k defines 
		a k-neighborhood as follows:
		N_k(index,length) = { (index',length'): |index-index'|+|index'+length'-(index+length)|<=k }."""
		self.id = id
		self.index = index
		self.start = self.index
		self.length = length
		self.end =self.index+self.length
		if k.isdigit():
			self.k = int(k)
		else:
			m = re.match(r"([\d\.]+).*",k)
			p = float(m.group(1))
			self.k = int(p*(length-1)/100)
	def __lt__(self, other): 
		return self.__cmp__(other) == -1
	def __eq__(self, other): return self.__cmp__(other) == 0
	def __cmp__(self, other):
		return cmp(self.leftmostStartPos(), other.leftmostStartPos()) or cmp(self.id, other.id)
	def __getitem__(self):
		return (self.id, self.index, self.length, self.k)
	def __str__(self):
		return '\t'.join(str(x) for x in [self. id,self.index, self.length, self.k])
	def leftmostStartPos(self):
		return max(0, self.index - self.k)
	def possibleLengths(self):
		return range(max(1,self.length-self.k), self.length+self.k+1)
	def lengthValid(self, length):
		return max(1,self.length - self.k) <= length <= self.length + self.k
	def possibleStartPositions(self, length):
		"""Returns (start_min, start_max) of allowed start positions for a given length,
		i.e. all values of start with start_min <= start <= start_max are valid start positions
		of insertions of the given length (INCLUSIVE)."""
		assert self.lengthValid(length)
		return max(0, int(math.ceil((2*self.index+self.length-length-self.k)/2))),  int(math.floor((2*self.index+self.length-length+self.k)/2))

class NeighborhoodInsertion:
	def __init__(self, id, index, seq, k):
		"""Models an ambiguous insertion where index and length are in "pythonic" coordinates. The parameter k defines 
		a k-neighborhood as follows:
		N_k(index,length) = { (index',length'): |index-index'|+|index'+length'-(index+length)|<=k }."""
		self.id = id
		self.index = index
		self.start = self.index
		self.seq = seq[1:len(seq)]
		self.length=len(seq)-1
		self.end =self.index+self.length
		if k.isdigit():
			self.k = int(k)
		else:
			m = re.match(r"([\d\.]+).*",k)
			p = float(m.group(1))
			self.k = int(p*(self.length-1)/100)
	def __lt__(self, other): 
		return self.__cmp__(other) == -1
	def __eq__(self, other): return self.__cmp__(other) == 0
	def __cmp__(self, other):
		return cmp(self.leftmostStartPos(), other.leftmostStartPos()) or cmp(self.id, other.id)
	def __getitem__(self):
		return (self.id, self.index, self.length, self.k, self.seq)
	def __str__(self):
		return '\t'.join(str(x) for x in [self. id,self.index,self.length, self.k,self.seq])
	def leftmostStartPos(self):
		return max(0, self.index - self.k)
	def possibleLengths(self):
		return range(max(1,self.length-self.k), self.length+self.k+1)
	def lengthValid(self, length):
		return max(1,self.length - self.k) <= length <= self.length + self.k
	def possibleStartPositions(self, length):
		"""Returns (start_min, start_max) of allowed start positions for a given length,
		i.e. all values of start with start_min <= start <= start_max are valid start positions
		of insertions of the given length (INCLUSIVE)."""
		if not self.lengthValid(length):
			print(length)
		assert self.lengthValid(length)
		return max(0, int(math.ceil((2*self.index+self.length-length-self.k)/2))),  int(math.floor((2*self.index+self.length-length+self.k)/2))

class WindowInsertion:
	def __init__(self, id, index_min, index_max, min_length, max_length):
		"""Models a insertion where
		start_min<=index <= start_max
		min_length <= length <= max_length."""
		self.id = id
		self.index=index_min
		self.index_min = index_min
		self.index_max = index_max
		self.min_length = min_length
		self.max_length = max_length
		self.length=min_length
	def __lt__(self, other): return self.__cmp__(other) == -1
	def __eq__(self, other): return self.__cmp__(other) == 0
	def __cmp__(self, other):
		return cmp(self.leftmostStartPos(), other.leftmostStartPos()) or cmp(self.id, other.id)
	def __str__(self):
		return '\t'.join(str(x) for x in [self.id,self.index_min,self.index_max,self.min_length,self.max_length])
	def leftmostStartPos(self):
		return self.index_min
	def possibleLengths(self):
		return range(self.min_length, self.max_length+1)
	def lengthValid(self, length):
		return self.min_length <= length <= self.max_length
	def possibleStartPositions(self, length):
		assert self.lengthValid(length)
		return self.index_min, self.index_max
        
class FuzzyInsertion:
	def __init__(self, id, index_min, index_max, length, k):
		"""Models a insertion where index lies in the given interval, i.e.
		index_min <= index <= index_max and 
		length is given"""
		self.id = id
		self.index=index_min
		self.index_min = index_min
		self.index_max = index_max
		self.length = int(length)
		self.k=int(k)
	def __lt__(self, other): return self.__cmp__(other) == -1
	def __eq__(self, other): return self.__cmp__(other) == 0
	def __cmp__(self, other):
		return cmp(self.leftmostStartPos(), other.leftmostStartPos()) or cmp(self.id, other.id)
	def __str__(self):
		return '\t'.join(str(x) for x in [self.id,self.index_min,self.index_max,self.length])
	def leftmostStartPos(self):
		return self.index_min
	def possibleLengths(self):
		return range(max(1, self.length-self.k), self.length+self.k+ 1) 
	def lengthValid(self, length):
		return max(1, self.length-self.k) <= length <= self.length+self.k+ 1
	def possibleStartPositions(self, length):
		assert self.lengthValid(length)
		return self.index_min, self.index_max






class Neighbour():
	def __init__(self,index, seq, k, restcosts=0):
		self.index=index
		self.seq=seq
		self.costs=k-restcosts
		self.lce=-1
		self.k=k
		self.restcosts=restcosts
		self.lce_rev=None
	def set_lce(self, lce):
		self.lce=lce
	def set_lce_rev(self, lce_rev):
		self.lce_rev=lce_rev
	def __str__(self):
		return '\t'.join(str(x) for x in ["index", self. index,"seq",self.seq,"costs", self.costs,"lce", self.lce])

def compute_stats(sequence, insertion1, insertion2):
	"""For a given pair of similar insertions, it returns the tupel (id of insertion 1, id of insertion 2, minimum required flexibility, minimum shift)."""
	# statistical  values are only defined for neighborhood insertions
	if not (insertion1.__class__.__name__=="NeighborhoodAmbInsertion" and insertion1.__class__.__name__=="NeighborhoodAmbInsertion"):
		return ("undef", "undef")
	min_K=None
	min_s=None
	# iterate over all lengths that can be in k1-neighborhood of [start1:end1] and k2-neighborhood of [start2:end2]
	for length in set(insertion1.possibleLengths()).intersection(set(insertion2.possibleLengths())):
		# compute range of possible start positions for this length
		unshifted_start_min1, unshifted_start_max1 = insertion1.possibleStartPositions(length)
		unshifted_start_min2, unshifted_start_max2 = insertion2.possibleStartPositions(length)
		# if start position too far right, we are done with this length
		shifted_start_max1 =  unshifted_start_max1 + lce(sequence, unshifted_start_max1, unshifted_start_max1 + length)
		shifted_end_max1=shifted_start_max1+length
		#if shifted_end_max1 < insertion2.leftmostStartPos():
			#continue
		# for ins1, start with rightmost position of minimum k (if you could shift a insertion starting from a position further to the left, you will walk along this one anyway.)
		unshifted_start_right = max(insertion1.index,insertion1.index+insertion1.length-length)
		# for ins1, start with leftmost position of minimum k (if you could shift a insertion starting from a position further to the right, you will walk along this one anyway.)
		unshifted_start_left = min(insertion2.index+insertion2.length-length,insertion2.index)
		# in case the deletions overlap, we cannot be such stringend, because overlap case would not be identified because second one is to far at the left side
		if unshifted_start_left < unshifted_start_right:
			unshifted_start_left = insertion2.index
			unshifted_start_right = insertion1.index
		# try different starts for ins1
		for unshifted_start_1 in range(unshifted_start_right,unshifted_start_max1+1):
			k1 = abs(unshifted_start_1-insertion1.index) + abs(unshifted_start_1+length - insertion1.end)
			# k1 already larger than minimum K? This and further starts cannot yield new optimum
			if not min_K==None and k1>min_K:
				break
			shifted_start_1 = unshifted_start_1 + lce(sequence, unshifted_start_1, unshifted_start_1 + length)
			shifted_end1 = shifted_start_1+length
			# try different starts for ins2
			for unshifted_start_2 in range(unshifted_start_left,unshifted_start_min2-1,-1):
				# if start position is in range, these two insertions are similar
				if unshifted_start_2 <= shifted_end1:
					# compute used k2 and total used K
					k2 = abs(unshifted_start_2-insertion2.index) + abs(unshifted_start_2+length - insertion2.end)
					K = k1+k2
					#hit -> update minima
					if min_K==None or K <= min_K:
						#compute shift
						s = abs(unshifted_start_2 - unshifted_start_1)
						if min_s==None or K < min_K or s < min_s:
							min_s=s
						min_K = K
	return (min_K, min_s)
	

def all_pot_similar_insertions_leftfirst(sequence, list1, list2, stats, ambi):
	"""Given two sorted lists of ambiguous insertions computes the set of all pairs of indices of potentially
	k1-k2-similar insertions for which the one in the first list is left of or at the same position as the
	one in the second list."""
	matches = set() # store just the matching pair
	result = set() # store more info
	if (len(list1) == 0) or (len(list2) == 0):
		return result
	i2 = 0
	insertion2 = list2[i2]
	
	for i1, insertion1 in enumerate(list1):
		while insertion2.index < insertion1.index:
			if i2 + 1 < len(list2):
				i2 += 1
				insertion2 = list2[i2]
			else:
				# if there are no insertions in list2 right of [start1:end1], we are done
				return result
		# iterate over all lengths that can be in k1-neighborhood of [start1:end1]
		for length in insertion1.possibleLengths():
			# compute range of possible start positions
			unshifted_start_min1, unshifted_start_max1 = insertion1.possibleStartPositions(length)
			shifted_start_max1 =  unshifted_start_max1 + lce(sequence, unshifted_start_max1, unshifted_start_max1 + length)
			end_max1=shifted_start_max1+length
			#for each i2 right of i1
			for j in range(i2, len(list2)):
				insertion_j = list2[j]

				# if start position too far right, we are done (for this i1) since all following insertion_j will be even further to the right
				if end_max1 < insertion_j.leftmostStartPos():
					break
				# if length is not compatible, we can skip this one
				if not insertion_j.lengthValid(length):
					continue
				
				unshifted_start_min2, unshifted_start_max2 = insertion_j.possibleStartPositions(length)
				# if start position is in range, these two insertions are similar
				if unshifted_start_min2 <= end_max1:
					# the same pair can match several times with different lengths (and thus shifts), so check for uniqueness
					if not matches.__contains__((i1,j, length)):
						matches.add((i1,j, length))
						if ambi:
							if stats:
								# compute minimum required sum of k1 and k2
								(K,s)=compute_stats(sequence, insertion1, insertion_j)
								result.add((i1,j, K,s))
							else: #stats and not ambi:
								result.add((i1, j))
						else: #stats are calculated later if needed
							result.add((i1, j, length))
	l = list(result)
	l.sort()
	return l
	
def dotString(length):
	result=""
	while(length>0):
		result+="."
		length-=1
	return result
	
def create_neighbours(insertion, l, min_k, ref_length=-1):
	result=set()
	#if min_k != None tpsyhere exists already a match with K=min_k and we do just search for better matches for stats
	if min_k != None and min_k < insertion.k:
		k=min_k
	else:
		k=insertion.k
	length=insertion.length
	seq=insertion.seq
	index=insertion.index
	minindex=0
	maxindex=ref_length
	restcosts=0;
	#l' is smaller than length of insertion
	if l <= length:
		i=0
		#if length-l < k-1:
		costs = k-(length-l)
		dots=0
		while(costs > 1 ):
			i-=1
			if (dots<l):
				dots+=1
			costs-=2
			if index+i >= minindex and index+i <= maxindex and l+i>=0 and l+i <= length:
				str=dotString(dots)+seq[0: l+i]
				if len(str) == l:
					restcosts=k-(dots+length-(l+i))
					result.add(Neighbour(index+i, str, k, restcosts))
			shift=0
			tmpcosts=costs
			while tmpcosts > 1:
				tmpcosts-=2
				shift+=1
				if index+i+shift >= minindex and index+i+shift <= maxindex and l+i>=0 and l+i <= length:
					str=dotString(dots)+seq[0: l+i]
					if len(str) == l:
						restcosts=k-(2*shift+dots+length-(l+i))
						result.add(Neighbour(index+i+shift, str,k,  restcosts))
				if index+i-shift >=minindex and index+i-shift <= maxindex and l+i>=0 and l+i <= length:
					str=dotString(dots)+seq[0: l+i]
					if len(str) == l:
						result.add(Neighbour(index+i-shift, str,k,  restcosts))
		i=0	
		while(l+i <= length):
			costs=k-(length-l)
			shift=0
			if index+i >= minindex and index+i <= maxindex and l+i <= length:
				str=seq[i: l+i]
				if len(str)==l:
					restcosts=k-(i+ length-(l+i))
					result.add(Neighbour(index+i, str, k, restcosts))
			while(costs>1):
				shift+=1
				if index+i+shift >= minindex and index+i+shift <= maxindex and l+i <= length:
					str= seq[i: l+i]
					if len(str)==l:
						restcosts=k-(2*shift+i+length-(l+i))
						result.add(Neighbour(index+i+shift,str, k, restcosts))
				if index+i-shift >= minindex and index+i-shift <= maxindex and l+i <= length:
					str=seq[i: l+i]
					if len(str)==l:
						result.add(Neighbour(index+i-shift, str,k,  restcosts))
				costs-=2
			i+=1
		i-=1
		
		
		if length-l < k-1:
			#add bases to the right
			costs = k-(length-l)
			dots=0
			while(costs > 1 ):
				i+=1
				if (dots<l):
					dots+=1
				costs-=2
				if index+i >= minindex and index+i-shift <= maxindex:
					str=seq[length-(l-dots): length]+dotString(dots)
					if len(str)==l:
						restcosts=k-(dots+length-(l-dots))
						result.add(Neighbour(index+i, str, k, restcosts))
				shift=0
				tmpcosts=costs
				while tmpcosts > 1:
					shift+=1
					tmpcosts-=2
					if index+i+shift >=  minindex and index+i+shift <= maxindex:
						str=seq[length-(l-dots): length]+dotString(dots)
						if len(str)==l:
							restcosts=k-(dots+length-(l-dots)+2*shift)
							result.add(Neighbour(index+i+shift,str,k,  restcosts))
					if index+i-shift >=  minindex and index+i-shift <= maxindex:
						str=seq[length-(l-dots): length]+dotString(dots)
						if len(str)==l:
							restcosts=k-(dots+length-(l-dots)+2*shift)
							result.add(Neighbour(index+i-shift, str, k, restcosts))
	else:
		i=l-length
		left=i
		costs = k-i
		if costs > 1:
			#add bases to the left and cut on the right
			dots=0
			while(costs > 1 ):
				i+=1
				if dots+left < l:
					dots+=1
				costs-=2
				if index-i >= minindex and index-i <= maxindex:
					restcosts=k-(2*dots+left)
					str=dotString(dots+left)+seq[0:length-dots]
					if len(str) == l:
						result.add(Neighbour(index-i, str,k,  restcosts))

						
				shift=0
				tmpcosts=costs
				while tmpcosts > 1:
					shift+=1
					tmpcosts-=2     
					if index-i+shift >=  minindex and index-i+shift <= maxindex:	
						str=dotString(dots+left)+seq[0:length-dots]
						if len(str) == l:
							restcosts=k-(2*dots+left+2*shift)
							result.add(Neighbour(index-i+shift, str,k,  restcosts))
					if index-i-shift >=  minindex and index-i-shift <= maxindex:
						str=dotString(dots+left)+seq[0:length-dots]
						if len(str) == l:
							restcosts=k-(2*dots+left+2*shift)
							result.add(Neighbour(index-i-shift, str,k,  restcosts))
		i=l-length
		
		while(i>=0):
			left=i
			right=l-left-length	
			#subsequences without cutting left or right
			costs=k-(l-length)
			shift=0
			if index-i >= minindex and index-i <= maxindex:
				str=dotString(left)+seq+dotString(right)
				if len(str) == l:
					restcosts=k-(left+right)
					result.add(Neighbour(index-i, str,k,  restcosts))
			while(costs>1):
				shift+=1
				costs-=2
				if index-i+shift >=  minindex and index-i+shift <= maxindex:
					str=dotString(left)+seq+dotString(right)
					if len(str) == l:
						restcosts=k-(left+right+2*shift)
						result.add(Neighbour(index-i+shift, str,k,  restcosts))
				if index-i-shift >=  minindex and index-i-shift <= maxindex:
					str=dotString(left)+seq+dotString(right)
					if len(str) == l:
						restcosts=k-(left+right+2*shift)
						result.add(Neighbour(index-i-shift, str,k,  restcosts))
			i-=1
	
		i=0
		left=0
		costs = k-(l-length)
		right=l-left-length	
		if costs > 1:
			#cut bases on the right
			dots=0
			while(costs > 1 ):
				i+=1
				if dots+right < l:
					dots+=1
				costs-=2
				if index+i >= minindex and index+i <= maxindex:
					str=seq[dots: length]+dotString(right+dots)
					if len(str) == l:
						restcosts=k-(2*dots+right)
						result.add(Neighbour(index+i, str,k,  restcosts))
				shift=0
				tmpcosts=costs
				while tmpcosts > 1:
					shift+=1
					tmpcosts-=2
					if index+i+shift >=  minindex and index+i+shift <= maxindex:
						str=seq[dots: length]+dotString(right+dots)
						if len(str) == l:
							restcosts=k-(2*dots+right+2*shift)
							result.add(Neighbour(index+i+shift,str,k,  restcosts))
					if index+i-shift >=  minindex and index+i+shift <= maxindex:
						str= seq[dots: length]+dotString(right+dots)
						if len(str) == l:
							restcosts=k-(2*dots+right+2*shift)
							result.add(Neighbour(index+i-shift,str,k,  restcosts))
	return result
	
def mincosts(ref, index1, seq1, index2, seq2, min_lce):
	min_lce=abs(min_lce)
	offset_K=0
	offset_K += lce_neighbour(ref, Neighbour(index1, seq1, 0, 0), min_lce)
	length=len(ref)
	revref=ref[::-1]
	revseq2=seq2[::-1]
	revindex2=length-index2
	offset_K += lce_neighbour(revref, Neighbour(revindex2, revseq2, 0, 0), min_lce)	
	return offset_K


def lce_neighbour(reference, neighbour, min_length=-1):
	"""Returns the length of the longest common extension of i and j,
	i.e., the largest k such that sequence[i:i+k] == sequence[j:j+k]."""
	k = 0
	i=0
	iseq=neighbour.seq
	j=neighbour.index
	restcosts=neighbour.restcosts
	fix = 0;
	counter=0
	if min_length!=-1:
		counter=min_length
		offset_k=0
		restcosts=min_length
	while j<len(reference) and (iseq[i] == reference[j] or iseq[i]=='.' or (restcosts>0 and fix==0)) and iseq[i]!='N' and reference[j]!='N' and (min_length==-1 or counter > 0):
		counter -= 1
		if (iseq[i]=='.'):
		
			if i==0:
				iseq=reference[j]+iseq[1:len(iseq)]
			elif i<len(iseq)-1:
				iseq=iseq[0:i]+reference[j]+iseq[i+1:len(iseq)]
			else:
				iseq=iseq[0:i]+reference[j]
		
		elif (iseq[i] != reference[j] and fix==0):
			restcosts-=1
			if min_length!=-1:
				offset_k += 1
			if i==0:
				iseq=reference[j]+iseq[1:len(iseq)]
			elif i<len(iseq)-1:
				iseq=iseq[0:i]+reference[j]+iseq[i+1:len(iseq)]
			else:
				iseq=iseq[0:i]+reference[j]
			
		i += 1
		j += 1
		k += 1	
		if i >=len(iseq):
			i=0
			fix=1
			
	if min_length != -1:
		return offset_k
	neighbour.set_lce(k)
	return neighbour
	


	

def neighbour_lces(ref, neighbours1, neighbours2, min2):
	"""Calculates the lce for each of the two lists inside the reference sequence. For the first list lce to the right is calculated
		and for the second list lce to the left(Considering sequences are inserted into the reference)"""
	result1=list()
	result2=list()
	max_start1=0
	min_start2=0
	revref=ref[::-1]
	length=len(ref)
	first=1
	for n in neighbours1:
		index=n.index
		sequence=n.seq
		n=(lce_neighbour(ref, n))
		if index+n.lce>=min2:
			if first:
				result1.append(n)
				first=0
			else:
				counter=0
				while counter < len(result1):
					if n.costs < result1[counter].costs:
						break
					counter=counter+1;
				result1.insert(counter, n)
			if max_start1==None or (index>max_start1) :
				max_start1=index
	
	first=1
	for n in neighbours2:
		index=n.index
		sequence=n.seq
		k=n.k
		revsequence=sequence[::-1]
		revindex=length-index
		restcosts=n.restcosts
		rev_n=lce_neighbour(revref, Neighbour(revindex, revsequence,k,  restcosts))
		if index-rev_n.lce<=max_start1:
			n.set_lce(rev_n.lce)
			if first:
				result2.append(n)
				first=0
			else:
				counter=0
				while counter < len(result2):
					if n.costs < result2[counter].costs:
						break
					counter=counter+1;
				result2.insert(counter, n)
			if min_start2==None or index<min_start2:
				min_start2=index
	return(result1, result2, min_start2)
	
	
	
def seq_equals(seq1, seq2, point, restcosts=0):
	if len(seq1) != len(seq2):
		print("can't cmp ", seq1, "and ", seq2, " unequal lengths",  file=sys.stderr)
		sys.exit(0)
	length = len(seq1)
	if(point):
		for i in range(0, length):
			if seq1[i] != seq2[i] and seq1[i] != '.' and seq2[i] != '.':
				if restcosts == 0:
					return 0
				else:
					restcosts-=1
	else:
		for i in range(0, length):
			if seq1[i] != seq2[i]:
				return 0
	return 1 
	
def all_similar_insertions_leftfirst(sequence, pot_similar_pairs,list1, list2,  stats, samelists):
	"""uses list of all potential similar pairs of ambiguous insertions and length of similar neighbourhood insertions ar a prefilter to find all similars pairs of insertions"""
	matches = set() # store just the matching pair
	result = set() # store more info
	dict_matches={}
	
	neighbours1=set()
	neighbours2=set()
	match=0
	min_k=None
	min_s=None

	#if pair is already a match we are done here, if stats, search for better stats
	for id_i1, id_i2, l in pot_similar_pairs:
		if  matches.__contains__((id_i1, id_i2)):
			if stats:
				(K, s)=dict_matches[(id_i1, id_i2)]
				min_k=K
				min_s=s
			else:
				continue
		else:
			min_k=None
			min_s=None
			
		
		if samelists:
			if not id_i1==id_i2:
				if match:
					match=0
			
				i1=list1[id_i1]
				i2=list2[id_i2]
				if i1.index > i2.index:
					print("insertions are in the false order", file=sys.stderr)
					sys.exit(1)
				#creates list of all neighbours, if min_k already exists for the pair, just neighbours with costs <= min_k (shift could be smaller than min_s)
				neighbours1 = create_neighbours(i1, l, min_k, len(sequence))
				neighbours2 = create_neighbours(i2, l, min_k, len(sequence))
	
				min2, max2=i2.possibleStartPositions(l)
				#neighbours come back sorted ascending by costs and with lce
				(neighbours1, neighbours2, min_start2)=neighbour_lces(sequence, neighbours1, neighbours2,min2)

				if len(neighbours1)!=0 and len(neighbours2)!=0:
					#list sorted by costs 
					for neighbour1 in neighbours1:
						costs1=neighbour1.costs
						#done if costs could not be smaller
						if min_k !=None and costs1 > min_k:
							break
						#true if match exists and stats=0
						if match:
							match=0
							break
						index1=neighbour1.index
						sequence1=neighbour1.seq
						lce1=neighbour1.lce
						#index too far away
						if(index1+lce1<min_start2):
							continue
						#also sorted by costs
						for neighbour2 in neighbours2:
							costs2=neighbour2.costs
							index2=neighbour2.index
							sequence2=neighbour2.seq
							lce2=neighbour2.lce
							#if costs too high done here
							if (min_k != None and costs1+costs2 > min_k) or (costs1+costs2 == min_k and abs(index2-index1) >= min_s):
								break							
						
							#if second neighbourhood insertion is left of the first, change insertions, calc lces to the other directions
							if (index2 < index1):
								ns1=set()
								if neighbour2.lce_rev==None:
									ns1.add(neighbour2)
								ns2=set()
								if neighbour1.lce_rev==None:
									ns2.add(neighbour1)
								(ns1, ns2, tmp)=neighbour_lces(sequence, ns1, ns2, min2)
								if len(ns1)==1:
									n1=ns1.pop()
								else:
									continue
								index1=n1.index
								sequence1=n1.seq
								lce1=n1.lce
								neighbour2.set_lce_rev(lce1)
								if len(ns2)==1:
									n2=ns2.pop()
								else:
									continue
								index2=n2.index
								sequence2=n2.seq
								lce2=n2.lce
								neighbour1.set_lce_rev(lce2)

							#inachbarschaftsinsertionen bilden keinen ueberlapp
							if index1+len(sequence1)<=index2:

								#match if they are shiftable into each other
								if index1+lce1>=index2 and index2-lce2<=index1:
									if stats:
										min_lce=index2-index1
										K=neighbour1.costs+neighbour2.costs
										s=index2-index1
										if min_lce > 0:
											K += mincosts(sequence, index1, sequence1, index2, sequence2, min_lce)
										if not matches.__contains__((id_i1, id_i2)) or K<min_k or (K==min_k and s<min_s):
											if not matches.__contains__((id_i1, id_i2)):
												matches.add((id_i1, id_i2))
												result.add((id_i1, id_i2, K, s))
												min_k=K
												min_s=s
												dict_matches.update({(id_i1, id_i2):(K, s)})
											else:
												min_k=K
												min_s=s
												dict_matches.update({(id_i1, id_i2):(K, s)})
									elif not matches.__contains__((id_i1, id_i2)):
											matches.add((id_i1, id_i2))
											result.add((id_i1, id_i2))
											match=1
											break
										
							#wenn nachbarschaftsinsertionen ueberlappen muessen ueberlappende sequenzen gleich sein
							else:	
								if index1+lce1>=index2 and index2-lce2<=index1:
									ov_l=(index1+l)-index2
									seq1=sequence1[l-ov_l:l]
									seq2=sequence2[0:ov_l]
									k1=neighbour1.costs
									k2=neighbour2.costs
									if seq_equals(seq1, seq2, 1, k1+k2):
										if stats:
											min_lce=index2-index1
											K=neighbour1.costs+neighbour2.costs
											s=index2-index1
											if min_lce > 0:
												K += mincosts(sequence, index1, sequence1, index2, sequence2, min_lce)
											if not matches.__contains__((id_i1, id_i2)) or K<min_k or (K==min_k and s<min_s):
												if not matches.__contains__((id_i1, id_i2)):
													matches.add((id_i1, id_i2))
													result.add((id_i1, id_i2, K, s))
													min_k=K
													min_s=s
													dict_matches.update({(id_i1, id_i2):(K, s)})
												else:
													min_k=K
													min_s=s
													dict_matches.update({(id_i1, id_i2):(K, s)})
										elif not matches.__contains__((id_i1, id_i2)):
												matches.add((id_i1, id_i2))
												result.add((id_i1, id_i2))
												match=1
												break
										
							index1=neighbour1.index
							costs1=neighbour1.costs
							sequence1=neighbour1.seq
							lce1=neighbour1.lce
		#if lists are different
		else:
			if  matches.__contains__((id_i1, id_i2)):
				if stats:
					(K, s)=dict_matches[(id_i1, id_i2)]
					min_k=K
					min_s=s
				else:
					continue
			else:
				min_k=None
				min_s=None
			if match:
					match=0
			i1=list1[id_i1]
			i2=list2[id_i2]
			if i1.index > i2.index:
				print("insertions are in false order",  file=sys.stderr)
				sys.exit(0)
			neighbours1 = create_neighbours(i1, l, min_k, len(sequence))
			neighbours2 = create_neighbours(i2, l, min_k, len(sequence))
			min2, max2=i2.possibleStartPositions(l)
			(neighbours1, neighbours2, min_start2)=neighbour_lces(sequence, neighbours1, neighbours2, min2)
			if len(neighbours1)!=0 and len(neighbours2)!=0:
					for neighbour1 in neighbours1:
						if match:
							match=0
							break
						index1=neighbour1.index
						sequence1=neighbour1.seq
						lce1=neighbour1.lce
						if(index1+lce1<min_start2):
							continue
						for neighbour2 in neighbours2:
						
							index2=neighbour2.index
							sequence2=neighbour2.seq
							lce2=neighbour2.lce
							
							#if second neighbourhood insertion is left of the first, change insertions
							if (index2 < index1):
								ns1=set()
								if neighbour2.lce_rev==None:
									ns1.add(neighbour2)
								ns2=set()
								if neighbour1.lce_rev==None:
									ns2.add(neighbour1)
								(ns1, ns2, tmp)=neighbour_lces(sequence, ns1, ns2, min2)
								if len(ns1)==1:
									n1=ns1.pop()
								else:
									continue
								index1=n1.index
								sequence1=n1.seq
								lce1=n1.lce
								neighbour2.set_lce_rev(lce1)
								if len(ns2)==1:
									n2=ns2.pop()
								else:
									continue
								index2=n2.index
								sequence2=n2.seq
								lce2=n2.lce
								neighbour1.set_lce_rev(lce2)
							#neighborhood insertions do not overlap
							if index1+len(sequence1)<=index2:
								#match if shiftable into each other
								if index1+lce1>=index2 and index2-lce2<=index1:
									if stats:
										min_lce=index2-index1
										K=neighbour1.costs+neighbour2.costs
										s=index2-index1
										if min_lce > 0:
											K += mincosts(sequence, index1, sequence1, index2, sequence2, min_lce)
										if not matches.__contains__((id_i1, id_i2)) or K<min_k or (K==min_k and s<min_s):
											if not matches.__contains__((id_i1, id_i2)):
												matches.add((id_i1, id_i2))
												result.add((id_i1, id_i2, K, s))
												min_k=K
												min_s=s
												dict_matches.update({(id_i1, id_i2):(K, s)})
											else:
												min_k=K
												min_s=s
												dict_matches.update({(id_i1, id_i2):(K, s)})
									elif not matches.__contains__((id_i1, id_i2)):
											matches.add((id_i1, id_i2))
											result.add((id_i1, id_i2))
											match=1
											break
							#if neighborhoosinsertions oberlap, overlapping sequences must be equal
							else:	
							
								if index1+lce1>=index2 and index2-lce2<=index1:
									ov_l=(index1+l)-index2
									seq1=sequence1[l-ov_l:l]
									seq2=sequence2[0:ov_l]
									k1=neighbour1.costs
									k2=neighbour2.costs
									if seq_equals(seq1, seq2, 1, k1+k2):
										if stats:
											min_lce=index2-index1
											K=neighbour1.costs+neighbour2.costs
											s=index2-index1
											if min_lce > 0:
												K += mincosts(sequence, index1, sequence1, index2, sequence2, min_lce)
											if not matches.__contains__((id_i1, id_i2)) or K<min_k or (K==min_k and s<min_s):
												if not matches.__contains__((id_i1, id_i2)):
													matches.add((id_i1, id_i2))
													result.add((id_i1, id_i2, K, s))
													min_k=K
													min_s=s
													dict_matches.update({(id_i1, id_i2):(K, s)})
												else:
													min_k=K
													min_s=s
													dict_matches.update({(id_i1, id_i2):(K, s)})
										elif not matches.__contains__((id_i1, id_i2)):
												matches.add((id_i1, id_i2))
												result.add((id_i1, id_i2))
												match=1
												break
	
	l=list()
	if stats:
		keys=dict_matches.keys()
		for key in keys:
			value=dict_matches[key]
			l.append((key[0], key[1], value[0], value[1]))
	else:
		l = list(result)
	l.sort()
	return l


def read_insertions(filename,fileformat,ambi, minimum,maximum,  default_k):
        """Reads insertions from a BED or VCF file and returns a dictionary mapping chromosome names to
            sorted lists of pairs of start and end coordinate."""
        if fileformat.name==None:
            raise Exception('Unknown input format for file %s. File must be a .bed file or .vcf file, or the format must be given explicitely with option --format(1|2).'%filename)		
        result = defaultdict(list)
        info_and_k_warning_printed = False
        if fileformat.name=='vcf' or fileformat.name=='windows':
            linenr = 0
            for line in open(filename):
                try:
                    linenr += 1
                    if line.startswith('#'):
                        continue
                    fields = line.split()
                    #assert muste be true, elso exception is thrown
                    assert len(fields) >= 8
                    chromosome, pos, id, ref, alt, qual, filter, info = fields[0], int(fields[1]), fields[2], fields[3], fields[4], fields[5], fields[6], fields[7]
                    #dict converts collection into dictionary, length and type
                    info_dict = dict(s.split('=') for s in info.split(';') if '=' in s)
                    if chromosome.lower().startswith('chr'):
                        chromosome=chromosome[3:]
                    if 'SVTYPE' in info_dict:
                            ins_start = pos
                            if info_dict['SVTYPE'] != 'INS':
                                continue
                            if not 'SVLEN' in info_dict:
                                if not 'END' in info_dict: 
                                        print('Warning: line %d of "%s" invalid (both SVLEN and END missing), skipping it'%(linenr,filename), file=sys.stderr)
                                        continue
                                else:
                                        ins_length = int(info_dict['END']) - ins_start + 1
                            else:
                                ins_length = abs(int(info_dict['SVLEN']))
                    else:
                        if len(ref)==1 and len(alt)>1:
                            ins_length=len(alt)-1
                            ins_start= pos
                        else:
                            continue
                    if not ambi and (not valid_dna_string(ref) or not valid_dna_string(alt)): 
                        print('Warning: line %d of "%s" invalid (REF or ALT contains non DNA characters), skipping it'%(linenr,filename), file=sys.stderr)
                        continue
                    if default_k != None:
                        if ('CILEN' in info_dict) and ('BPWINDOW' in info_dict):
                            if not info_and_k_warning_printed:
                                print('Warning: Line %d of "%s" gives information on insertion uncertainty, but is overridden by k given on command line.'%(linenr,filename), file=sys.stderr)
                                print('         Subsequent warnings will be suppressed', file=sys.stderr)
                                info_and_k_warning_printed = True
                        if ambi:
                            add_insertion(result[chromosome],NeighborhoodAmbInsertion(linenr, ins_start, ins_length, default_k), minimum, maximum)
                        else:
                            add_insertion(result[chromosome],NeighborhoodInsertion(linenr, ins_start, alt, default_k), minimum, maximum)
                    elif ('CILEN' in info_dict) and ('BPWINDOW' in info_dict):
                        min_length, max_length = (int(x) for x in info_dict['CILEN'].split(','))
                        assert min_length <= max_length
                        index_min, index_max = (int(x) for x in info_dict['BPWINDOW'].split(','))
                        assert min_length <= ins_length < max_length, "Insertion length not in length window given by CILEN in line %d"%linenr
                        add_insertion(result[chromosome],WindowInsertion(linenr, index_min, index_max, min_length, max_length), minimum, maximum)
                except ValueError:
                    raise Exception('Bad input file. Offending line: %s'%linenr)
        elif fileformat.name == 'breakdancer' or fileformat.name=='breakdancer_fuzzy':
            linenr = 0
            for line in open(filename):
                try:
                    linenr += 1
                    if line.startswith('#'):
                        continue
                    fields = line.split()
                    assert len(fields) >= 10
                    chr1, pos1, chr2, pos2, type, ins_length = fields[0], int(fields[1]), fields[3], int(fields[4]), fields[6], abs(int(fields[7]))
                    if type !='INS':
                        continue
                    assert chr1==chr2

                    if chr1.lower().startswith('chr'):
                        chr1=chr1[3:]
                    if fileformat.name=='breakdancer':
                        if not info_and_k_warning_printed:
                            print('Warning: Line %d of "%s" gives information on insertion uncertainty, but is overridden by k given on command line. For index mean value is taken'%(linenr,filename), file=sys.stderr)
                            print('         Subsequent warnings will be suppressed', file=sys.stderr)
                            info_and_k_warning_printed = True
                        ins_start=int((pos1+pos2)/2)
                        add_insertion(result[chr1],NeighborhoodAmbInsertion(linenr, ins_start, ins_length, default_k), minimum, maximum)
                    else:
                        add_insertion(result[chr1],FuzzyInsertion(linenr, pos1, pos2, ins_length, default_k), minimum, maximum)
                except ValueError:
                    print(line)
                    raise Exception('Bad input file. Offending line: %s'%linenr)
        elif fileformat.name =='pindel' or fileformat.name=='pindel_fuzzy':
            linenr = 0
            for line in open(filename):
                try:
                    linenr += 1
                    fields = line.split("\t")
                    if len(fields) != 20:
                        continue                    
                    id, type, seq, chromosome, index_min, index_max= fields[0], fields[1], fields[2], fields[3], fields[6], fields[7]
                    length=int(type.split(" ")[1])
                    type=type.split(" ")[0]
                    #add one base because ass_insertion function cuts first base because of vcf input format
                    seq="A"+ seq.split(" ")[2].replace("\"", "")
                    chromosome=chromosome.split(" ")[1]
                    if chromosome.lower().startswith('chr'):
                        chromosome=chromosome[3:]
                    index_min=int(index_min.split(" ")[1])
                    index_max=int(index_max)
                    if fileformat.name=='pindel':
                        if ambi:
                            add_insertion(result[chromosome],NeighborhoodAmbInsertion(linenr, index_min,  length, default_k), minimum, maximum)
                        if not ambi:
                            if not valid_dna_string(seq): 
                                print('Warning: line %d of "%s" invalid (REF or ALT contains non DNA characters), skipping it'%(linenr,filename), file=sys.stderr)
                                continue
                            add_insertion(result[chromosome],NeighborhoodInsertion(linenr, index_min, seq, default_k), minimum, maximum)
                    else:
                       add_insertion(result[chromosome],FuzzyInsertion(linenr, index_min, index_max, length, default_k), minimum, maximum)
                        
                except ValueError:
                    raise Exception('Bad input file. Offending line: %s'%linenr)


        print('%d insertions read from file %s.'%(sum(len(x) for y,x in result.items()),filename), file=sys.stderr)
        return result



#if __name__ == '__main__':
	## Parse Arguments
	#args,options = parse_options()
	#ambiguous = options.ambiguous
	## iterate over various k if necessary
	## create a list of k values, even if no range is given
	
	#if options.minimum > 2:
		#print("minimum insertion length of ", options.minimum, " is used" , file=sys.stderr)
	#if options.maximum != 10000:
		#print("maximum insertion length of ", options.maximum, " is used" , file=sys.stderr)
	#name1=options.inputformat1.name
	#name2=options.inputformat2.name
	#if name1=='windows' or name2=='windows'  or name1 =='breakdancer' or name2=='breakdancer' or name1=='breakdancer_fuzzy' or name2=='breakdancer_fuzzy' or name1=='pindel_fuzzy' or name2=='pindel_fuzzy':
		#ambiguous=True
	## Read reference sequence
	#reference=read_reference(args[0])
	#k1list=get_list_of_ks(options.k1)
	#k2list=get_list_of_ks(options.k2)
	##for each k value combination (if --k2=k1 then k values are equals)
	#for k1 in k1list:
		#if options.k2 == "k1":
			#k2list=[k1]
		#for k2 in k2list:
			## handle comparison of list with itself separately
			#if args[1] == args[2]:
				#if k1==k2:
					#insertions1 = read_insertions(args[1], options.inputformat1, ambiguous,options.minimum, options.maximum, k1)
					#insertions2 = insertions1
				
				#else:
					#insertions1 = read_insertions(args[1], options.inputformat1, ambiguous, options.minimum,options.maximum,  k1)
					#insertions2 = read_insertions(args[1], options.inputformat1, ambiguous, options.minimum,options.maximum,  k2)

				
				#chromosomes = set(insertions1.keys()).intersection(set(insertions2.keys()))
				#for chromosome in chromosomes:
					#if not chromosome in reference:
						#print('Skipping insertions for chromosome "%s": reference unknown.'%chromosome, file=sys.stderr)
						#continue
					#print('Processing insertions on chromosome "%s".'%chromosome, file=sys.stderr)
					#sequence=reference[chromosome]
					#insertion_list1 = insertions1[chromosome]
					#insertion_list2 = insertions2[chromosome]
					##includes all insertions from the given chromosome
					#pot_similar_pairs1 = set()
					#pot_similar_pairs2 = set()           
					#similar_pairs = set()        
					                                                 
					#pot_similar_pairs1.update(all_pot_similar_pairs_leftfirst(sequence, insertion_list1, insertion_list2, options.stats, ambiguous))
					#pot_similar_pairs_list1=list(pot_similar_pairs1)
					#pot_similar_pairs_list1.sort()
					
					
					#if not ambiguous:
						#similar_pairs.update(all_similar_pairs_leftfirst(sequence, pot_similar_pairs_list1, insertion_list1, insertion_list2, options.stats, 1))
					## get all similar pairs for which the insertion with k2 is further left (or at same position)
					#if k1!=k2:
						#if ambiguous:
							#if options.stats:
								#pot_similar_pairs2.update((i2,i1, K,s) for i2, i1,  K, s in all_pot_similar_pairs_leftfirst(sequence, insertion_list2, insertion_list1, options.stats, ambiguous))
							#else:
								#pot_similar_pairs2.update((i2,i1) for i2, i1 in all_pot_similar_pairs_leftfirst(sequence,  insertion_list2, insertion_list1, options.stats, ambiguous))
						#else:
							#pot_similar_pairs2.update((i2,i1, l) for i2, i1, l in all_pot_similar_pairs_leftfirst(sequence,  insertion_list2, insertion_list1, options.stats, ambiguous))
							#pot_similar_pairs_list2=list(pot_similar_pairs2)
							#pot_similar_pairs_list2.sort()
							#if options.stats:
								#similar_pairs.update((i1, i2, K, s) for i2, i1, K, s in  all_similar_pairs_leftfirst(sequence, pot_similar_pairs_list2, insertion_list2, insertion_list1, options.stats, 1))
							#else:
								#similar_pairs.update((i1, i2) for i2, i1 in  all_similar_pairs_leftfirst(sequence, pot_similar_pairs_list2, insertion_list2, insertion_list1, options.stats, 1))
				
					#result_list=list()
					#if ambiguous:
						#pot_similar_pairs1.update(pot_similar_pairs2)
						#pot_similar_pairs_list = list(pot_similar_pairs1)
						#pot_similar_pairs_list.sort()
						#result_list=pot_similar_pairs_list
						#if options.stats:
							#for i1, i2, K, s in result_list:
								#if i1!=i2: 
									#print(chromosome, insertion_list1[i1], insertion_list2[i2],  s,  K,  sep='\t')
						#else:
							#for i1, i2 in result_list:
								#if i1!=i2: 
									#print(chromosome, insertion_list1[i1], insertion_list2[i2], sep='\t')
					#else:
						#similar_pairs_list=list(similar_pairs)
						#similar_pairs_list.sort()
						#result_list=similar_pairs_list
						#if options.stats:
							#for i1, i2,  K, s in result_list:
								#if i1!=i2: 
									#print(chromosome, insertion_list1[i1], insertion_list2[i2], s,  K,  sep='\t')
						#else:
							#for i1, i2 in result_list:
								#if i1!=i2: 
									#print(chromosome, insertion_list1[i1], insertion_list2[i2],  sep='\t')



			#else:
				#insertions1 = read_insertions(args[1], options.inputformat1,ambiguous, options.minimum,options.maximum,  k1)
				#insertions2 = read_insertions(args[2], options.inputformat2,ambiguous, options.minimum,options.maximum,  k2)
				## set of chromosome names appearing in both insertion lists
				#chromosomes = set(insertions1.keys()).intersection(set(insertions2.keys()))
				#for chromosome in chromosomes:
					#if not chromosome in reference:
						#print('Skipping insertions for chromosome "%s": reference unknown.'%chromosome, file=sys.stderr)
						#continue
					#print('Processing insertions on chromosome "%s".'%chromosome, file=sys.stderr)
					#sequence=reference[chromosome]
					#insertion_list1 = insertions1[chromosome]
					#insertion_list2 = insertions2[chromosome]
					#pot_similar_pairs1 = set()
					#pot_similar_pairs2 = set()           
					#similar_pairs = set()   
					## get all potentially similar pairs for which the insertion from list1 is further left (or at same position)
					#pot_similar_pairs1.update(all_pot_similar_pairs_leftfirst(sequence, insertion_list1, insertion_list2, options.stats, ambiguous))
					#pot_similar_pairs_list1=list(pot_similar_pairs1)
					#pot_similar_pairs_list1.sort()
					#if not ambiguous:
						#similar_pairs.update(all_similar_pairs_leftfirst(sequence, pot_similar_pairs_list1, insertion_list1, insertion_list2, options.stats, 0))
					## get all similar pairs for which the insertion from list2 is further left (or at same position)
					## that means, the ones at the same position were already returned by last call to all_pot_similar_pairs_leftfirst
					## (which does not matter as pot_similar_pairs is a set).
					#if ambiguous:
						#if options.stats:
							#pot_similar_pairs2.update((i1,i2, K,s) for i2, i1,  K, s in all_pot_similar_pairs_leftfirst(sequence, insertion_list2, insertion_list1, options.stats, ambiguous))
						#else:
							#pot_similar_pairs2.update((i1,i2) for i2, i1 in all_pot_similar_pairs_leftfirst(sequence,  insertion_list2, insertion_list1, options.stats, ambiguous))
					#else:
						#pot_similar_pairs2.update((i2,i1, l) for i2, i1, l in all_pot_similar_pairs_leftfirst(sequence,  insertion_list2, insertion_list1, options.stats, ambiguous))
						#pot_similar_pairs_list2=list(pot_similar_pairs2)
						#pot_similar_pairs_list2.sort()
						#if options.stats:
							#similar_pairs.update((i1, i2, K, s) for i2, i1, K, s in  all_similar_pairs_leftfirst(sequence, pot_similar_pairs_list2, insertion_list2, insertion_list1, options.stats, 1))
						#else:
							#similar_pairs.update((i1, i2) for i2, i1 in  all_similar_pairs_leftfirst(sequence, pot_similar_pairs_list2, insertion_list2, insertion_list1, options.stats, 1))
					#result_list=list()
					#if ambiguous:
						#pot_similar_pairs1.update(pot_similar_pairs2)
						#pot_similar_pairs_list = list(pot_similar_pairs1)
						#pot_similar_pairs_list.sort()
						#result_list=pot_similar_pairs_list
						#if options.stats:
							#for i1, i2,  K, s in result_list:
								#print(chromosome, insertion_list1[i1], insertion_list2[i2], s,  K,  sep='\t')
						#else:
							#for i1, i2 in result_list:
								#print(chromosome, insertion_list1[i1], insertion_list2[i2],    sep='\t')
					#else:
						#similar_pairs_list=list(similar_pairs)
						#similar_pairs_list.sort()
						#result_list=similar_pairs_list
						#if options.stats:
							#for i1, i2, K, s in result_list:
								#print(chromosome, insertion_list1[i1], insertion_list2[i2],  s,  K,  sep='\t')
						#else:
							#for i1, i2 in result_list:
								#print(chromosome, insertion_list1[i1], insertion_list2[i2],   sep='\t')
					

