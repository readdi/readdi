#!/usr/bin/env python3
from optparse import OptionParser
import sys
import os
import math
import time
import operator
from collections import defaultdict
from maxmatching import bipartiteMatch
from similarity import *

usage = """%prog [options] <reference.fasta> <indels1.(bed|vcf|...)> <dindels2.(bed|vcf|...)>

	Outputs true and false positives and negatives in terms of (k1,k2)-similarity for lists indels1 and indels2.
	Optional output of mapping in tab-separated format:
	Chrom.; Indel1: line no. in indels1, start, end, k1; Indel2: line no. in indels2, start, end, k2; DEL|INS"""

def graph_from_list(list):
	graph = {}
	for i1, i2 in list:
		if i1 not in graph:
			graph[i1]=[i2]
		else:
			graph[i1].append(i2)
	return graph


def open_matching_file(filename):
	"""Initializes the file to write the matching to."""
	if filename == "":
		return None
	else:
		f=open(os.path.expanduser(filename), 'w')
		return f
	
def close_matching_file(file):
	"""closes the file to write the matching to."""
	if not file is None:
		file.close

def write_matching(filename, deletion_list1, deletion_list2, chromosome, matching):
	"""Writes matching deletions into the given file. """
	if not filename is None:
		for v in matching:
			u=matching[v]
			print(chromosome, deletion_list1[u], deletion_list2[v], sep='\t', file=filename)


def compute_statistics_from_similarity_lists(indels_list1, indels_list2, similar_indels_list_12, similar_indels_11, similar_indels_22, chromosome, matching_file=None):
	"""computes TP, FP, FN, SP, SN, FNC, FPC for given lists of deletions or insertions (used only for one chromosome).
	   Lists of indels are expected to be not sorted yet"""

	# init statistics
	TP=0
	FP=0
	FN=0
	SP=0
	SN=0
	FNC=0
	FPC=0

	similar_indels_list_12.sort()
	
	# create graph
	graph = graph_from_list(similar_indels_list_12)
	# maximum matching
	(matching,x,y)=bipartiteMatch(graph)
	#output
	if matching_file!=None:
		write_matching(matching_file,indels_list1, indels_list2, chromosome, matching)

	### STATISTICS
	
	TP+=len(matching)

	# get SPs ("similar positives")
	# get all similar pairs within set 1
	SP_set= set() # collect all dels similar to any TP
	FPC_assignment={} # del -> clique
	FPC_inv={} # clique -> [dels]
	for i in range(len(indels_list1)):
		FPC_assignment[i]=i
		FPC_inv[i]=[i]
	for i1,i2 in similar_indels_11:
		if i1==i2:
			continue
		# SPs
		if i1 in matching.values() and not i2 in matching.values():
			SP_set.add(i2)
		# update FPCs: put all elements from the clique containing i2 to the clique containing i1
		c1=FPC_assignment[i1]
		c2=FPC_assignment[i2]
		if c1!=c2:
			for d in FPC_inv[c2]:
				FPC_assignment[d]=c1
			FPC_inv[c1].extend(FPC_inv[c2])
			del(FPC_inv[c2])

	# remove from FPC_inv cliques containing TPs
	for d in matching.values():
		if FPC_assignment[d] in FPC_inv:
			del(FPC_inv[FPC_assignment[d]])
	
	FPC+=len(FPC_inv)			
	SP+=len(SP_set)
	FP+=len(indels_list1)-len(matching)-len(SP_set)

	# get SNs ("similar negatives")
	# get all similar pairs within set 2
	SN_set= set() # collect all dels similar to any TP
	FNC_assignment={} # del -> clique
	FNC_inv={} # clique -> [dels]
	for i in range(len(indels_list2)):
		FNC_assignment[i]=i
		FNC_inv[i]=[i]
	for i1, i2 in similar_indels_22:
		if i1==i2:
			continue
		# SPs
		if i1 in matching and not i2 in matching:
			SN_set.add(i2)
		# update FNCs: put all elements from the clique containing i2 to the clique containing i1
		c1=FNC_assignment[i1]
		c2=FNC_assignment[i2]
		if c1!=c2:
			for d in FNC_inv[c2]:
				FNC_assignment[d]=c1
			FNC_inv[c1].extend(FNC_inv[c2])
			del(FNC_inv[c2])

	# remove from FNC_inv cliques containing TPs
	for d in matching:
		if FNC_assignment[d] in FNC_inv:
			del(FNC_inv[FNC_assignment[d]])

	FNC+=len(FNC_inv)			
	SN+=len(SN_set)
	FN+=len(indels_list2)-len(matching)-len(SN_set)

	return (TP,FP,FN,SP,SN,FNC,FPC)
   
	   


def compute_statistics(indels1, indels2, reference, chromosomes, ambiguous,  matching_file=None):
	"""computes TP, FP, FN, SP, SN, FNC, FPC for given lists of deletions considering only the specified chromosomes"""
	
	(deletions1, insertions1, ins_sequence_given1)=indels1
	(deletions2, insertions2, ins_sequence_given2)=indels2
	
	ambiguous=ambiguous or not ins_sequence_given1 or not ins_sequence_given1
		
	# init statistics
	total_stats_del=(0,0,0,0,0,0,0)
	total_stats_ins=(0,0,0,0,0,0,0)

	for chromosome in chromosomes:

		if not chromosome in reference:
			print('Skipping deletions for chromosome "%s": reference unknown.'%chromosome, file=sys.stderr)
			continue

		# DELETIONS
		print('Processing deletions on chromosome "%s".'%chromosome, file=sys.stderr)
		deletion_list1 = deletions1[chromosome]
		deletion_list2 = deletions2[chromosome]
		similar_deletions_12 = set()
		# get all similar pairs for which the deletion from list1 is further left (or at same position)
		similar_deletions_12.update(all_similar_deletions_leftfirst(reference[chromosome], deletion_list1, deletion_list2, False))
		# get all similar pairs for which the deletion from list2 is further left (or at same position)
		similar_deletions_12.update((i1,i2) for i2, i1 in all_similar_deletions_leftfirst(reference[chromosome], deletion_list2, deletion_list1, False))
		similar_deletions_list_12 = list(similar_deletions_12)


		# get all similar pairs within set 1 and within set 2
		similar_deletions_11 = all_similar_deletions_leftfirst(reference[chromosome], deletion_list1, deletion_list1, False)
		similar_deletions_22 = all_similar_deletions_leftfirst(reference[chromosome], deletion_list2, deletion_list2, False)

		# do the matching and get statistics
		new_stats_del=compute_statistics_from_similarity_lists(deletion_list1, deletion_list2, similar_deletions_list_12, similar_deletions_11, similar_deletions_22, chromosome, matching_file)
		total_stats_del = tuple(map(operator.add, total_stats_del, new_stats_del))


		# INSERTIONS
		print('Processing insertions on chromosome "%s".'%chromosome, file=sys.stderr)
		insertion_list1 = insertions1[chromosome]
		insertion_list2 = insertions2[chromosome]
		similar_amb_insertions_12 = set()
		# get all similar pairs for which the insertion from list1 is further left (or at same position)
		similar_amb_insertions_12.update(all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list1, insertion_list2, False, ambiguous))
		# get all similar pairs for which the insertion from list2 is further left (or at same position)
		if ambiguous:
			similar_amb_insertions_12.update((i1,i2) for i2, i1 in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list1, False, ambiguous))
		else:
			similar_amb_insertions_12_inv = set()
			similar_amb_insertions_12_inv.update((i2,i1,l) for i2, i1, l in all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list1, False, ambiguous))
			similar_insertions_list_12_inv = list(similar_amb_insertions_12_inv)
			similar_insertions_list_12_inv.sort()

		similar_insertions_list_12 = list(similar_amb_insertions_12)
		similar_insertions_list_12.sort()


		# get all similar pairs within set 1 and within set 2
		similar_insertions_11 = all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list1, insertion_list1, False, ambiguous)
		similar_insertions_list_11 = list(similar_insertions_11)
		similar_insertions_list_11.sort()
		similar_insertions_22 = all_pot_similar_insertions_leftfirst(reference[chromosome], insertion_list2, insertion_list2, False, ambiguous)
		similar_insertions_list_22 = list(similar_insertions_22)
		similar_insertions_list_22.sort()


		if not ambiguous:
			# Unambiguous insertions: filter previous results
			similar_insertions_12 = set()
			similar_insertions_12.update(all_similar_insertions_leftfirst(reference[chromosome], similar_insertions_list_12, insertion_list1, insertion_list2, False, False))
			similar_insertions_12.update((i1,i2) for i2, i1 in all_similar_insertions_leftfirst(reference[chromosome], similar_insertions_list_12_inv, insertion_list2, insertion_list1, False, False))
			similar_insertions_list_12=list(similar_insertions_12)
			similar_insertions_11 = set()
			similar_insertions_11.update(all_similar_insertions_leftfirst(reference[chromosome], similar_insertions_list_11, insertion_list1, insertion_list1, False, True))
			similar_insertions_22 = set()
			similar_insertions_22.update(all_similar_insertions_leftfirst(reference[chromosome], similar_insertions_list_22, insertion_list2, insertion_list2, False, True))


		# do the matching and get statistics
		new_stats_ins=compute_statistics_from_similarity_lists(insertion_list1, insertion_list2, similar_insertions_list_12, similar_insertions_11, similar_insertions_22, chromosome, matching_file)
		total_stats_ins = tuple(map(operator.add, total_stats_ins, new_stats_ins))



	return (total_stats_del,total_stats_ins)

if __name__ == '__main__':

	ts = time.time()

	# Parse Arguments with extra option
	args,options = parse_options_k1k2(usage,3,[(["--matching"], {"action":"store", "dest":"matching_filename", "default":"", "type":"string", "help":"File to store the matching indels."})])

	# Read reference sequence
	reference=read_reference(args[0])

	matching_file=open_matching_file(options.matching_filename)

	# iterate over various k if necessary
	# create a list of k values, even if no range is given
	k1list=get_list_of_ks(options.k1)
	k2list=get_list_of_ks(options.k2)

	#output header
	print("k1\tk2\tTP\tFP\tFN\tSP\tSN\tFNC\tFPC\tTIME\tTYPE")

	#output time info
	print('***','time:','k1','k2', sep="\t", file=sys.stderr)
	print('time for reading input:',time.time()-ts, sep="\t", file=sys.stderr)
	ts = time.time()
	
	for k1 in k1list:
		if options.k2 == "k1":
			k2list=[k1]
		for k2 in k2list:

			indels1 = read_indels(args[1], options.inputformat1, k1, options.ambiguous)
			indels2 = read_indels(args[2], options.inputformat2, k2, options.ambiguous)
			
			(deletions1, insertions1, ins_sequence_given1)=indels1
			(deletions2, insertions2, ins_sequence_given2)=indels2

			
			# set of chromosome names appearing in both indel lists
			chromosomes = set(deletions1.keys()).intersection(set(deletions2.keys())).union(set(insertions1.keys()).union(set(insertions2.keys())))
			
			#DELETIONS
			#compute statistics 
			((TP_del,FP_del,FN_del,SP_del,SN_del,FNC_del,FPC_del),(TP_ins,FP_ins,FN_ins,SP_ins,SN_ins,FNC_ins,FPC_ins)) = compute_statistics(indels1, indels2, reference, chromosomes, options.ambiguous, matching_file)
			print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(k1,k2,TP_del,FP_del,FN_del,SP_del,SN_del,FNC_del,FPC_del,"NA","DEL"))
			print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(k1,k2,TP_ins,FP_ins,FN_ins,SP_ins,SN_ins,FNC_ins,FPC_ins,"NA","INS"))


			# BOTH
			print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(k1,k2,TP_del+TP_ins,FP_del+FP_ins,FN_del+FN_ins,SP_del+SP_ins,SN_del+SN_ins,FNC_del+FNC_ins,FPC_del+FPC_ins,time.time()-ts,"BOTH"))
			ts = time.time()


	close_matching_file(matching_file)
