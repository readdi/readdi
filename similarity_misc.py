#!/usr/bin/env python3


def lce(sequence, i, j):
	"""Returns the length of the longest common extension of i and j,
	i.e., the largest k such that sequence[i:i+k] == sequence[j:j+k]."""
	k = 0
	while (max(i,j) < len(sequence)) and (sequence[i] == sequence[j] and sequence[i]!='N' and sequence[j]!='N'):
		i += 1
		j += 1
		k += 1
	return k

def cmp(a,b):
	return (a > b) - (a < b)